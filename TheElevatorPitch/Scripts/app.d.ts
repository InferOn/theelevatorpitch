declare namespace TheElevatorPitchModule {
    class Boot extends Phaser.State {
        preload(): void;
        create(): void;
    }
}
declare namespace TheElevatorPitchModule {
}
declare namespace TheElevatorPitchModule {
    class CompanyServiceProvider {
        private president;
        private staffServiceProvider;
        private productBuilder;
        private game;
        private utils;
        private dayLight;
        private currentDate;
        company: Company;
        constructor(game: Phaser.Game, utils: Utils, dayLight: number, currentDate: Date);
        Create(): void;
        Update(): void;
        putOnTop(): void;
        private init();
        private CalculateMontlyBalance();
        protected intInRange(min: number, max: number): number;
        private addProducts();
        private createStaff();
        private createCompany();
        private createPresident();
    }
}
declare namespace TheElevatorPitchModule {
    class Balance {
        Value: number;
        Outcome: number;
        Income: number;
    }
}
declare namespace TheElevatorPitchModule {
    class Company {
        Name: string;
        Staff: Staff[];
        currentDate: Date;
        Products: ProductLine.Product[];
        balance: Balance;
        Capital: number;
        constructor();
        CalculateMontlyBalance(): number;
        Update(currentDate: Date): void;
        addProduct(product: ProductLine.Product): void;
        addStaffMember(member: Staff): void;
        protected intInRange(min: number, max: number): number;
        Init(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class Customer {
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        abstract class Product {
            Name: string;
            Version: Version;
            Customers: Customer[];
            constructor();
            Upgrade(version: Version): void;
        }
        class ProductOptions {
            type: eTypeProduct;
            Version: Version;
            constructor(type: eTypeProduct);
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        enum eTypeProduct {
            CRM = 1,
            CMS = 2,
            BPM = 3,
            ORM = 4,
            SAAS = 5,
        }
        enum eTypePhase {
            Alpha = 1,
            Beta = 2,
            Stable = 3,
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class BPM extends Product {
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class CMS extends Product {
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class CRM extends Product {
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class ORM extends Product {
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class Phase {
            TypePhase: eTypePhase;
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class SAAS extends Product {
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class Version {
            Major: number;
            Minor: number;
            Build: number;
            constructor(typePhase: eTypePhase);
            protected intInRange(min: number, max: number): number;
            toString: () => string;
        }
    }
}
declare namespace TheElevatorPitchModule {
    namespace ProductLine {
        class ProductServiceProvider {
            createProduct(options: ProductOptions): Product;
            private applyVersion(product, version);
            private upgradeProduct(product, version);
        }
    }
}
declare namespace TheElevatorPitchModule {
    class DayNightServiceProviderOption {
        BackgroundSprites: any[];
        SunSprite: Phaser.Sprite;
        MoonSprite: Phaser.Sprite;
        StarsSprite: Phaser.Sprite;
        MeteoServiceProvider: MeteoServiceProvider;
        constructor(backgroundSprites: any[], sunSprite: Phaser.Sprite, moonSprite: Phaser.Sprite, starsSprite: Phaser.Sprite, meteoServiceProvider: MeteoServiceProvider);
    }
    class DayNightServiceProvider {
        private game;
        private dayLenght;
        private shading;
        private sunSprite;
        private moonSprite;
        private starsSprite;
        private sunTween;
        private moonTween;
        private starTween;
        private utils;
        private cloudServiceProvider;
        private meteoServiceProvider;
        private trafficServiceProvider;
        private Hour;
        private TimeLapse;
        private ClockTime;
        private TimerEvent;
        private clockText;
        CurrentDate: Date;
        motionSpeed: number;
        SunRiseSignal: Phaser.Signal;
        ClockSignal: Phaser.Signal;
        constructor(game: Phaser.Game, dayLenght: number, utils: Utils, options: DayNightServiceProviderOption, trafficServiceProvider: TrafficServiceProvider);
        initClockText(): void;
        initSun(sprite: Phaser.Sprite): void;
        initMoon(moon: Phaser.Sprite): void;
        sunRise(sprite: Phaser.Sprite): void;
        sunSet(sprite: Phaser.Sprite): void;
        moonRise(sprite: Phaser.Sprite): void;
        moonSet(moon: Phaser.Sprite): void;
        initShading(sprites: any[]): void;
        Update(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class CloudServiceProvider {
        private game;
        private utils;
        private meteoContitions;
        private dayLight;
        private clouds;
        constructor(game: Phaser.Game, utils: Utils, dayLight: number);
        Update(meteoContitions: MeteoResult): void;
        protected intInRange(min: number, max: number): number;
    }
}
declare namespace TheElevatorPitchModule {
    class MeteoResult {
        conditions: eMeteoConditions;
        spawnCloud: boolean;
        shouldUpdate: boolean;
        tint: boolean;
        cloudSpeed: number;
        constructor();
    }
    class SeasonInfo {
        Name: string;
        StartMonth: number;
        constructor(name: string, startMonth: number);
    }
}
declare namespace TheElevatorPitchModule {
    enum eMeteoConditions {
        Sunny = 0,
        ModeratelyCloudly = 1,
        Cloudly = 2,
        Shitty = 3,
        Snow = 4,
    }
}
declare namespace TheElevatorPitchModule {
    class MeteoPredictor {
        private seasons;
        private game;
        private dateDay;
        private duration;
        private day;
        private condition;
        private spawnCloud;
        private cloudSpeed;
        private tint;
        constructor(game: Phaser.Game);
        Predict(date: Date, hour: number): MeteoResult;
        protected intInRange(min: number, max: number): number;
        setCondition(): void;
        setClouds(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class MeteoServiceProvider {
        private game;
        private utils;
        private currentCondition;
        private meteoPredictor;
        private dayLight;
        CloudServiceProvider: CloudServiceProvider;
        RainServiceProvider: RainServiceProvider;
        private MeteoText;
        constructor(game: Phaser.Game, utils: Utils, cloudServiceProvider: CloudServiceProvider, rainServiceProvider: RainServiceProvider, dayLight: number);
        initMeteoText(): void;
        Update(date: Date, hour: number): void;
        putOnTop(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class RainServiceProvider {
        private game;
        private utils;
        private emitter;
        private bitmapData;
        private dayLight;
        constructor(game: Phaser.Game, utils: Utils, dayLight: number);
        Update(meteoContitions: MeteoResult): void;
        putOnTop(): void;
        private addTear(bitmapData, rainLenght, x, y);
        Start(): void;
        protected intInRange(min: number, max: number): number;
        Stop(): void;
    }
}
declare namespace TheElevatorPitchModule {
    interface IUpdatable {
        Update(): void;
    }
    abstract class Staff implements IUpdatable {
        DOB: Date;
        Health: number;
        Sprite: Phaser.Sprite;
        Name: string;
        SenseOfHumor: number;
        Gender: eGender;
        Salary: number;
        protected CurrentDate: Date;
        constructor(currentDate: Date);
        protected intInRange(min: number, max: number): number;
        private _initStats();
        private initHealth();
        private initSenseOfHumor();
        private initGender();
        private initName();
        protected capitalizeFirstLetter(string: any): any;
        private initDOB();
        getAge(currentDate: Date): number;
        protected abstract initStats(): void;
        WakeUp(): void;
        Tac(hour: number): void;
        Update(): void;
    }
}
declare namespace TheElevatorPitchModule {
    enum eTypeStaff {
        Programmer = 1,
        ProjectManager = 2,
        Tester = 3,
        BusinnessAnalist = 4,
        CTO = 5,
        CEO = 6,
    }
    enum eGender {
        Male = 1,
        Female = 2,
    }
}
declare namespace TheElevatorPitchModule {
    class BusinnessAnalist extends Staff {
        constructor(currentDate: Date);
        protected initStats(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class CEO extends Staff {
        constructor(currentDate: Date);
        protected initStats(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class CTO extends Staff {
        constructor(currentDate: Date);
        protected initStats(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class Programmer extends Staff implements IUpdatable {
        private ProblemSolving;
        private Programming;
        private Debugging;
        constructor(currentDate: Date);
        protected initStats(): void;
        Update(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class ProjectManager extends Staff {
        constructor(currentDate: Date);
        protected initStats(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class Tester extends Staff {
        constructor(currentDate: Date);
        protected initStats(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class StaffFactory {
        private currentDate;
        constructor(currentDate: Date, game: Phaser.Game);
        protected intInRange(min: number, max: number): number;
        CreateStaff(type: eTypeStaff): Staff;
    }
}
declare namespace TheElevatorPitchModule {
    class StaffServiceProvider {
        private game;
        private utils;
        private dayLight;
        private currentDate;
        private staffFactory;
        constructor(game: Phaser.Game, utils: Utils, dayLight: number, currentDate: Date);
        Update(currentDate: Date): void;
        CreateStaff(type: eTypeStaff): Staff;
        RemoveStaff(staff: Staff): void;
    }
}
declare namespace TheElevatorPitchModule {
    class TheCity extends Phaser.State {
        private backgroundSprite;
        private sunSprite;
        private moonSprite;
        private starsSprite;
        private skyline;
        private skyscraper;
        private officebackground;
        private lights;
        private utils;
        private meteoCondition;
        private dayNightServiceProvider;
        private meteoServiceProvider;
        private trafficServiceProvider;
        private companyServiceProvider;
        preload(): void;
        create(): void;
        update(): void;
        private registerEvents();
        private fetchImages();
        private setDayAndNight();
        private setBackgroundSprite();
        private setSkyscraper();
        private setSkyline();
        private setLights();
        private giveMeBackground(width, height);
        private giveMeBackgroundSprites();
        private putOnTop();
    }
}
declare namespace TheElevatorPitchModule {
    class CarBuilder {
        game: Phaser.Game;
        dayLenght: number;
        options: CarOptions;
        utils: Utils;
        constructor(game: Phaser.Game, utils: Utils, dayLenght: number, options: CarOptions);
        Create(): Phaser.Sprite;
        private RGBtoHEX(r, g, b);
        protected intInRange(min: number, max: number): number;
    }
}
declare namespace TheElevatorPitchModule {
    class CarOptions {
        prevX: number;
        Y: number;
        AnchorX: number;
        NextX: number;
        NextW: number;
        NextH: number;
        ScaleX: number;
        PutOnTop: boolean;
        Speed: number;
        Easing: any;
        Selector: string;
        constructor(prevX: number, Y: number, AnchorX: number, NextX: number, NextW: number, NextH: number, ScaleX: number, PutOnTop: boolean, Speed: number, Easing: any, Selector: string);
    }
}
declare namespace TheElevatorPitchModule {
    class TrafficIntervall {
        limit: number;
        condition: () => boolean;
        constructor(condition: () => boolean, limit: number);
    }
}
declare namespace TheElevatorPitchModule {
    class TrafficServiceProvider {
        private game;
        private utils;
        private dayLenght;
        private carSprites;
        private autobus;
        private autobusTween;
        private CurrentDate;
        constructor(game: Phaser.Game, utils: Utils, dayLenght: number);
        private spawn56(date, hour);
        protected intInRange(min: number, max: number): number;
        private spawnTraffic(options);
        Update(date: Date, hour: number): void;
        destroyAutobus(autobus: Phaser.Sprite): void;
        putOnTop(): void;
    }
}
declare namespace TheElevatorPitchModule {
    class UpdateTrafficOptions {
        date: Date;
        hour: number;
        putOnTop: boolean;
        intervals: TrafficIntervall[];
        constructor(date: Date, hour: number, putOnTop: boolean);
    }
}
declare namespace TheElevatorPitchModule {
    class Utils {
        private game;
        constructor(game: Phaser.Game);
        tweenTint(sprite: Phaser.Sprite, startColor: number, endColor: number, duration: number, easing: any): void;
        RGBtoHEX(r: number, g: number, b: number): number;
        capitalizeFirstLetter(string: any): any;
    }
}
