var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
        };
        Boot.prototype.create = function () {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.state.start("TheCity", true, false);
        };
        return Boot;
    }(Phaser.State));
    TheElevatorPitchModule.Boot = Boot;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
/// <reference path="Ts/Boot.ts" />
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    var TheElevatorPitch = (function (_super) {
        __extends(TheElevatorPitch, _super);
        function TheElevatorPitch() {
            _super.call(this, "100%", "100%", Phaser.AUTO, 'content', null);
            this.state.add('Boot', TheElevatorPitchModule.Boot, false);
            this.state.add('TheCity', TheElevatorPitchModule.TheCity, false);
            this.state.start("Boot", false, false);
            try {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            }
            catch (ex) {
                console.error(ex);
            }
        }
        return TheElevatorPitch;
    }(Phaser.Game));
    window.onload = function () {
        var game = new TheElevatorPitch();
    };
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var CompanyServiceProvider = (function () {
        function CompanyServiceProvider(game, utils, dayLight, currentDate) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
            this.currentDate = currentDate;
            this.productBuilder = new TheElevatorPitchModule.ProductLine.ProductServiceProvider();
            this.staffServiceProvider = new TheElevatorPitchModule.StaffServiceProvider(this.game, this.utils, this.dayLight, this.currentDate);
        }
        CompanyServiceProvider.prototype.Create = function () {
            this.init();
        };
        CompanyServiceProvider.prototype.Update = function () {
            this.putOnTop();
            this.company.Update(this.currentDate);
        };
        CompanyServiceProvider.prototype.putOnTop = function () {
        };
        CompanyServiceProvider.prototype.init = function () {
            this.createCompany();
            this.createPresident();
            this.createStaff();
            this.addProducts();
            console.log(this.company);
        };
        CompanyServiceProvider.prototype.CalculateMontlyBalance = function () {
            this.company.CalculateMontlyBalance();
        };
        CompanyServiceProvider.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        CompanyServiceProvider.prototype.addProducts = function () {
            var options = new TheElevatorPitchModule.ProductLine.ProductOptions(this.intInRange(1, TheElevatorPitchModule.ProductLine.eTypeProduct.SAAS));
            options.Version = new TheElevatorPitchModule.ProductLine.Version(this.intInRange(1, TheElevatorPitchModule.ProductLine.eTypePhase.Stable));
            this.company.addProduct(this.productBuilder.createProduct(options));
        };
        CompanyServiceProvider.prototype.createStaff = function () {
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(TheElevatorPitchModule.eTypeStaff.ProjectManager));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(TheElevatorPitchModule.eTypeStaff.BusinnessAnalist));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(TheElevatorPitchModule.eTypeStaff.Tester));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(TheElevatorPitchModule.eTypeStaff.Programmer));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(TheElevatorPitchModule.eTypeStaff.Programmer));
        };
        CompanyServiceProvider.prototype.createCompany = function () {
            this.company = new TheElevatorPitchModule.Company();
            this.company.Init();
        };
        CompanyServiceProvider.prototype.createPresident = function () {
            var self = this;
            var president = self.staffServiceProvider.CreateStaff(TheElevatorPitchModule.eTypeStaff.CEO);
            var gender = "male";
            if (president.Gender === TheElevatorPitchModule.eGender.Female) {
                gender = "female";
            }
            $.ajax({
                url: 'https://randomuser.me/api?gender=' + gender,
                dataType: 'json',
                success: function (data) {
                    president.Name = self.utils.capitalizeFirstLetter(data.results[0].name.first);
                    self.president = president;
                    self.company.Name = self.utils.capitalizeFirstLetter(data.results[0].name.last) + " Corporation";
                    self.company.Staff.push(president);
                },
                error: function () {
                    self.company.Name = "Evil Corporation";
                    self.company.Staff.push(president);
                }
            });
        };
        return CompanyServiceProvider;
    }());
    TheElevatorPitchModule.CompanyServiceProvider = CompanyServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Balance = (function () {
        function Balance() {
            this.Value = 0;
            this.Outcome = 0;
            this.Income = 0;
        }
        return Balance;
    }());
    TheElevatorPitchModule.Balance = Balance;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Company = (function () {
        function Company() {
            this.Name = "";
            this.Staff = [];
            this.currentDate = undefined;
            this.Products = [];
            this.balance = new TheElevatorPitchModule.Balance();
        }
        Company.prototype.CalculateMontlyBalance = function () {
            var expense = 0;
            this.Staff.forEach(function (element, index, staff) {
                var tempExpense = element.Salary / 14;
                tempExpense += ((tempExpense * 40) / 100);
                expense += tempExpense;
            });
            this.Products.forEach(function (element, index, products) {
            });
            return expense;
        };
        Company.prototype.Update = function (currentDate) {
            this.currentDate = currentDate;
            this.Staff.forEach(function (staff, index, array) {
                staff.Update();
            });
        };
        Company.prototype.addProduct = function (product) {
            this.Products.push(product);
        };
        Company.prototype.addStaffMember = function (member) {
            this.Staff.push(member);
        };
        Company.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        Company.prototype.Init = function () {
            this.Capital = this.intInRange(1, 10) * 1000;
        };
        return Company;
    }());
    TheElevatorPitchModule.Company = Company;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Customer = (function () {
        function Customer() {
        }
        return Customer;
    }());
    TheElevatorPitchModule.Customer = Customer;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var Product = (function () {
            function Product() {
                this.Customers = [];
            }
            Product.prototype.Upgrade = function (version) {
                this.Version = version;
            };
            return Product;
        }());
        ProductLine.Product = Product;
        var ProductOptions = (function () {
            function ProductOptions(type) {
                this.type = type;
            }
            return ProductOptions;
        }());
        ProductLine.ProductOptions = ProductOptions;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        (function (eTypeProduct) {
            eTypeProduct[eTypeProduct["CRM"] = 1] = "CRM";
            eTypeProduct[eTypeProduct["CMS"] = 2] = "CMS";
            eTypeProduct[eTypeProduct["BPM"] = 3] = "BPM";
            eTypeProduct[eTypeProduct["ORM"] = 4] = "ORM";
            eTypeProduct[eTypeProduct["SAAS"] = 5] = "SAAS";
        })(ProductLine.eTypeProduct || (ProductLine.eTypeProduct = {}));
        var eTypeProduct = ProductLine.eTypeProduct;
        (function (eTypePhase) {
            eTypePhase[eTypePhase["Alpha"] = 1] = "Alpha";
            eTypePhase[eTypePhase["Beta"] = 2] = "Beta";
            eTypePhase[eTypePhase["Stable"] = 3] = "Stable";
        })(ProductLine.eTypePhase || (ProductLine.eTypePhase = {}));
        var eTypePhase = ProductLine.eTypePhase;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var BPM = (function (_super) {
            __extends(BPM, _super);
            function BPM() {
                _super.apply(this, arguments);
            }
            return BPM;
        }(ProductLine.Product));
        ProductLine.BPM = BPM;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var CMS = (function (_super) {
            __extends(CMS, _super);
            function CMS() {
                _super.apply(this, arguments);
            }
            return CMS;
        }(ProductLine.Product));
        ProductLine.CMS = CMS;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var CRM = (function (_super) {
            __extends(CRM, _super);
            function CRM() {
                _super.apply(this, arguments);
            }
            return CRM;
        }(ProductLine.Product));
        ProductLine.CRM = CRM;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var ORM = (function (_super) {
            __extends(ORM, _super);
            function ORM() {
                _super.apply(this, arguments);
            }
            return ORM;
        }(ProductLine.Product));
        ProductLine.ORM = ORM;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var Phase = (function () {
            function Phase() {
            }
            return Phase;
        }());
        ProductLine.Phase = Phase;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var SAAS = (function (_super) {
            __extends(SAAS, _super);
            function SAAS() {
                _super.apply(this, arguments);
            }
            return SAAS;
        }(ProductLine.Product));
        ProductLine.SAAS = SAAS;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var Version = (function () {
            function Version(typePhase) {
                var _this = this;
                this.Major = 0;
                this.Minor = 0;
                this.Build = 0;
                this.toString = function () {
                    return _this.Major + "." + _this.Minor + "." + _this.Build;
                };
                switch (typePhase) {
                    case ProductLine.eTypePhase.Alpha:
                        this.Minor = 1;
                        this.Build = this.intInRange(100, 1000);
                        break;
                    case ProductLine.eTypePhase.Beta:
                        this.Minor = 9;
                        break;
                    case ProductLine.eTypePhase.Stable:
                        this.Major = 1;
                        break;
                }
            }
            Version.prototype.intInRange = function (min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            };
            return Version;
        }());
        ProductLine.Version = Version;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProductLine;
    (function (ProductLine) {
        var ProductServiceProvider = (function () {
            function ProductServiceProvider() {
            }
            ProductServiceProvider.prototype.createProduct = function (options) {
                var result;
                switch (options.type) {
                    case ProductLine.eTypeProduct.BPM:
                        result = new ProductLine.BPM();
                        break;
                    case ProductLine.eTypeProduct.CMS:
                        result = new ProductLine.CMS();
                        break;
                    case ProductLine.eTypeProduct.CRM:
                        result = new ProductLine.CRM();
                        break;
                    case ProductLine.eTypeProduct.ORM:
                        result = new ProductLine.ORM();
                        break;
                    case ProductLine.eTypeProduct.SAAS:
                        result = new ProductLine.SAAS();
                        break;
                }
                this.applyVersion(result, options.Version);
                return result;
            };
            ProductServiceProvider.prototype.applyVersion = function (product, version) {
                if (version != null && version != undefined) {
                    product.Version = version;
                }
                else {
                    product.Version = new ProductLine.Version(ProductLine.eTypePhase.Alpha);
                }
            };
            ProductServiceProvider.prototype.upgradeProduct = function (product, version) {
                product.Upgrade(version);
            };
            return ProductServiceProvider;
        }());
        ProductLine.ProductServiceProvider = ProductServiceProvider;
    })(ProductLine = TheElevatorPitchModule.ProductLine || (TheElevatorPitchModule.ProductLine = {}));
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var DayNightServiceProviderOption = (function () {
        function DayNightServiceProviderOption(backgroundSprites, sunSprite, moonSprite, starsSprite, meteoServiceProvider) {
            this.BackgroundSprites = backgroundSprites;
            this.SunSprite = sunSprite;
            this.MoonSprite = moonSprite;
            this.StarsSprite = starsSprite;
            this.MeteoServiceProvider = meteoServiceProvider;
        }
        return DayNightServiceProviderOption;
    }());
    TheElevatorPitchModule.DayNightServiceProviderOption = DayNightServiceProviderOption;
    var DayNightServiceProvider = (function () {
        function DayNightServiceProvider(game, dayLenght, utils, options, trafficServiceProvider) {
            this.Hour = 0;
            this.CurrentDate = new Date();
            this.SunRiseSignal = new Phaser.Signal();
            this.ClockSignal = new Phaser.Signal();
            this.game = game;
            this.utils = utils;
            this.dayLenght = dayLenght;
            this.shading = null;
            this.sunSprite = null;
            this.moonSprite = null;
            this.starsSprite = options.StarsSprite;
            this.TimeLapse = this.dayLenght / 12;
            this.motionSpeed = dayLenght + (dayLenght / 32);
            this.trafficServiceProvider = trafficServiceProvider;
            this.meteoServiceProvider = options.MeteoServiceProvider;
            this.initClockText();
            this.initShading(options.BackgroundSprites);
            this.initSun(options.SunSprite);
            this.initMoon(options.MoonSprite);
        }
        DayNightServiceProvider.prototype.initClockText = function () {
            this.clockText = this.CurrentDate.toDateString() + " " + (this.Hour < 10 ? "0" + this.Hour : this.Hour + "") + ":00";
            this.ClockTime = this.game.add.text(10, 10, this.clockText, {});
            this.ClockTime.fontSize = 12;
            this.ClockTime.addColor("white", 0);
        };
        DayNightServiceProvider.prototype.initSun = function (sprite) {
            this.sunSprite = sprite;
            this.sunRise(sprite);
        };
        DayNightServiceProvider.prototype.initMoon = function (moon) {
            this.moonSprite = moon;
            this.moonSet(moon);
        };
        DayNightServiceProvider.prototype.sunRise = function (sprite) {
            var _this = this;
            this.CurrentDate.setDate(this.CurrentDate.getDate() + 1);
            this.SunRiseSignal.dispatch(this.CurrentDate.getDate());
            sprite.position.x = this.game.width - sprite.width - (sprite.width / 2);
            this.sunTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.Out, true);
            this.utils.tweenTint(sprite, 0xff6600, 0xffff00, this.motionSpeed, Phaser.Easing.Default);
            this.sunTween.onComplete.add(this.sunSet, this);
            if (this.TimerEvent == null) {
                this.TimerEvent = this.game.time.events.loop(this.TimeLapse, function () {
                    if (_this.Hour === 24) {
                        _this.Hour = 0;
                    }
                    else {
                        _this.Hour++;
                        _this.ClockSignal.dispatch(_this.Hour);
                    }
                    _this.clockText = (_this.Hour < 10 ? "0" + _this.Hour : _this.Hour + "") + ":00";
                    _this.ClockTime.text = _this.CurrentDate.toDateString() + " " + _this.clockText;
                });
            }
            if (this.shading) {
                this.shading.forEach(function (sprite) {
                    _this.utils.tweenTint(sprite.sprite, sprite.from, sprite.to, _this.motionSpeed, Phaser.Easing.Linear.None);
                });
            }
        };
        DayNightServiceProvider.prototype.sunSet = function (sprite) {
            var _this = this;
            sprite.position.x = sprite.width + (sprite.width / 2);
            this.sunTween = this.game.add.tween(sprite)
                .to({ y: this.game.world.height + (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.In, true);
            this.utils.tweenTint(sprite, 0xffff00, 0xff6600, this.dayLenght, Phaser.Easing.Circular.In);
            this.sunTween.onComplete.add(this.sunRise, this);
            if (this.shading) {
                this.shading.forEach(function (sprite) {
                    _this.utils.tweenTint(sprite.sprite, sprite.to, sprite.from, _this.motionSpeed, Phaser.Easing.Circular.In);
                });
            }
        };
        DayNightServiceProvider.prototype.moonRise = function (sprite) {
            sprite.position.x = this.game.width - sprite.width - (sprite.width / 2);
            this.moonTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.Out, true);
            this.moonTween.onComplete.add(this.moonSet, this);
        };
        DayNightServiceProvider.prototype.moonSet = function (moon) {
            moon.position.x = moon.width + (moon.width / 2);
            this.moonTween = this.game.add.tween(moon)
                .to({ y: this.game.world.height + (moon.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.In, true);
            this.moonTween.onComplete.add(this.moonRise, this);
        };
        DayNightServiceProvider.prototype.initShading = function (sprites) {
            this.shading = sprites;
        };
        DayNightServiceProvider.prototype.Update = function () {
            this.trafficServiceProvider.Update(this.CurrentDate, this.Hour);
            this.meteoServiceProvider.Update(this.CurrentDate, this.Hour);
        };
        return DayNightServiceProvider;
    }());
    TheElevatorPitchModule.DayNightServiceProvider = DayNightServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var CloudServiceProvider = (function () {
        function CloudServiceProvider(game, utils, dayLight) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
            this.clouds = [];
        }
        CloudServiceProvider.prototype.Update = function (meteoContitions) {
            this.meteoContitions = meteoContitions;
            if (this.meteoContitions.spawnCloud) {
                var selector = "cloud" + this.intInRange(1, 6);
                var cloud = this.game.cache.getImage(selector);
                var isRightToLeft = this.intInRange(1, 2) === 2;
                var perc = this.intInRange(20, 40);
                var nextW = (cloud.width * perc) / 100;
                var nextH = (cloud.height * perc) / 100;
                var prevX = 0 - nextW - 50;
                var nextX = this.game.width + 50;
                if (isRightToLeft) {
                    prevX = this.game.width + nextW;
                    nextX = 0 - nextW;
                }
                var sprite = this.game.add.sprite(prevX, this.intInRange(-10, (this.game.height / 2)), selector);
                this.clouds.push(sprite);
                sprite.width = nextW;
                sprite.height = nextH;
                sprite.alpha = this.intInRange(80, 90);
                sprite.anchor.set(isRightToLeft ? 1 : 0, sprite.anchor.y);
                var startTint = this.utils.RGBtoHEX(255, 255, 255);
                sprite.tint = startTint;
                var endTint = this.utils.RGBtoHEX(0, 0, 0);
                var speed = this.intInRange(this.dayLight * 4, this.dayLight * 4);
                var cloudSpeedEasing = Phaser.Easing.Linear.None;
                var cloudTintEasing = Phaser.Easing.Default;
                if (this.meteoContitions.cloudSpeed > 0) {
                    speed = this.meteoContitions.cloudSpeed;
                    cloudSpeedEasing = Phaser.Easing.Linear.None;
                    cloudTintEasing = Phaser.Easing.Exponential.Out;
                }
                if (this.meteoContitions.tint) {
                    this.utils.tweenTint(sprite, startTint, endTint, speed, cloudTintEasing);
                }
                this.game.add.tween(sprite).to({ x: nextX }, speed, cloudSpeedEasing, true)
                    .onComplete.add(function (s, t) {
                    s.destroy();
                });
            }
        };
        CloudServiceProvider.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        return CloudServiceProvider;
    }());
    TheElevatorPitchModule.CloudServiceProvider = CloudServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var MeteoResult = (function () {
        function MeteoResult() {
            this.cloudSpeed = 0;
            this.shouldUpdate = true;
        }
        return MeteoResult;
    }());
    TheElevatorPitchModule.MeteoResult = MeteoResult;
    var SeasonInfo = (function () {
        function SeasonInfo(name, startMonth) {
            this.Name = name;
            this.StartMonth = startMonth;
        }
        return SeasonInfo;
    }());
    TheElevatorPitchModule.SeasonInfo = SeasonInfo;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    (function (eMeteoConditions) {
        eMeteoConditions[eMeteoConditions["Sunny"] = 0] = "Sunny";
        eMeteoConditions[eMeteoConditions["ModeratelyCloudly"] = 1] = "ModeratelyCloudly";
        eMeteoConditions[eMeteoConditions["Cloudly"] = 2] = "Cloudly";
        eMeteoConditions[eMeteoConditions["Shitty"] = 3] = "Shitty";
        eMeteoConditions[eMeteoConditions["Snow"] = 4] = "Snow"; // tbd
    })(TheElevatorPitchModule.eMeteoConditions || (TheElevatorPitchModule.eMeteoConditions = {}));
    var eMeteoConditions = TheElevatorPitchModule.eMeteoConditions;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var MeteoPredictor = (function () {
        function MeteoPredictor(game) {
            this.dateDay = undefined;
            this.duration = 0;
            this.day = 0;
            this.game = game;
            this.seasons = [];
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Spring", 3));
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Summer", 6));
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Autumn", 9));
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Winter", 12));
        }
        MeteoPredictor.prototype.Predict = function (date, hour) {
            var result = new TheElevatorPitchModule.MeteoResult();
            var start = false;
            var currentDay = date.getDate();
            if (this.dateDay === undefined) {
                this.dateDay = currentDay;
                this.setCondition();
                result.shouldUpdate = true;
                start = true;
            }
            if (this.dateDay !== currentDay) {
                this.day++;
                this.dateDay = currentDay;
                if (this.duration === this.day) {
                    this.day = 0;
                    this.setCondition();
                    result.shouldUpdate = true;
                }
            }
            else {
                if (!start)
                    result.shouldUpdate = false;
            }
            result.conditions = this.condition;
            this.setClouds();
            result.cloudSpeed = this.cloudSpeed;
            result.spawnCloud = this.spawnCloud;
            result.tint = this.tint;
            return result;
        };
        MeteoPredictor.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        MeteoPredictor.prototype.setCondition = function () {
            this.duration = this.intInRange(1, this.intInRange(3, 10));
            var prob = this.intInRange(1, 100);
            if (prob <= 10) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.Sunny;
            }
            else if (prob > 10 && prob <= 40) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.ModeratelyCloudly;
            }
            else if (prob > 40 && prob <= 60) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.Cloudly;
            }
            else if (prob > 60) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.Shitty;
            }
            // shitty mode
            //this.duration = 1;
            //this.condition = eMeteoConditions.Shitty;
        };
        MeteoPredictor.prototype.setClouds = function () {
            this.tint = false;
            if (this.condition == TheElevatorPitchModule.eMeteoConditions.Sunny) {
                this.spawnCloud = this.intInRange(1, 10000) < 10;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.ModeratelyCloudly) {
                this.spawnCloud = this.intInRange(1, 10000) < 100;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.Cloudly) {
                this.spawnCloud = this.intInRange(1, 10000) < 500;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.Shitty) {
                this.spawnCloud = this.intInRange(1, 10000) < 8000;
                this.cloudSpeed = 10000;
                this.tint = true;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.Snow) {
                this.spawnCloud = this.intInRange(1, 100000) < 10;
            }
        };
        return MeteoPredictor;
    }());
    TheElevatorPitchModule.MeteoPredictor = MeteoPredictor;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var MeteoServiceProvider = (function () {
        function MeteoServiceProvider(game, utils, cloudServiceProvider, rainServiceProvider, dayLight) {
            this.dayLight = dayLight;
            this.game = game;
            this.utils = utils;
            this.CloudServiceProvider = cloudServiceProvider;
            this.RainServiceProvider = rainServiceProvider;
            this.meteoPredictor = new TheElevatorPitchModule.MeteoPredictor(game);
            this.initMeteoText();
        }
        MeteoServiceProvider.prototype.initMeteoText = function () {
            this.MeteoText = this.game.add.text(10, 30, "Meteo: /", {});
            ;
            this.MeteoText.fontSize = 12;
            this.MeteoText.addColor("white", 0);
        };
        MeteoServiceProvider.prototype.Update = function (date, hour) {
            var result = this.meteoPredictor.Predict(date, hour);
            var meteoMsg = ["Sunny", "Moderately Cloudly", "Cloudly", "Shitty", "Snow"];
            this.MeteoText.text = "Meteo: " + meteoMsg[result.conditions];
            this.CloudServiceProvider.Update(result);
            if (result.shouldUpdate) {
                this.RainServiceProvider.Update(result);
            }
        };
        MeteoServiceProvider.prototype.putOnTop = function () {
            this.RainServiceProvider.putOnTop();
        };
        return MeteoServiceProvider;
    }());
    TheElevatorPitchModule.MeteoServiceProvider = MeteoServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var RainServiceProvider = (function () {
        function RainServiceProvider(game, utils, dayLight) {
            this.emitter = null;
            this.bitmapData = null;
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
        }
        RainServiceProvider.prototype.Update = function (meteoContitions) {
            if (meteoContitions.conditions === TheElevatorPitchModule.eMeteoConditions.Shitty) {
                this.Start();
            }
            else {
                this.Stop();
            }
        };
        RainServiceProvider.prototype.putOnTop = function () {
            if (this.emitter != null) {
                this.game.world.bringToTop(this.emitter);
            }
        };
        RainServiceProvider.prototype.addTear = function (bitmapData, rainLenght, x, y) {
            bitmapData.ctx.rect(x, y, 5, rainLenght);
            bitmapData.ctx.fillStyle = "#9cc9de";
            bitmapData.ctx.fill();
        };
        RainServiceProvider.prototype.Start = function () {
            var rainLenght = 2000;
            if (this.emitter == null) {
                this.emitter = this.game.add.emitter(this.game.world.centerX, 0, 1500);
                this.bitmapData = this.game.add.bitmapData(this.game.world.width, rainLenght + 50);
            }
            this.emitter.width = this.game.world.width + (this.game.world.width / 2);
            if (this.emitter.on == false) {
                this.addTear(this.bitmapData, rainLenght, 0, 0);
                var p = 0;
                var constant = 200;
                for (var i = constant; i < this.emitter.width; i += constant) {
                    this.addTear(this.bitmapData, rainLenght, i, 0);
                }
                this.emitter.width = this.game.world.width + (this.game.world.width / 2);
                this.emitter.minParticleScale = 0.1;
                this.emitter.maxParticleScale = 0.1;
                this.emitter.setYSpeed(2000, 3000);
                var dir = this.intInRange(1, 2);
                var minAngle, maxAngle;
                if (dir == 1) {
                    minAngle = -20;
                    maxAngle = -10;
                    this.emitter.pivot.x = 0;
                }
                else {
                    minAngle = 10;
                    maxAngle = 20;
                    this.emitter.pivot.x = 1;
                }
                this.emitter.angle = this.intInRange(minAngle, maxAngle);
                this.emitter.minRotation = -20;
                this.emitter.maxRotation = 20;
            }
            this.emitter.makeParticles(this.bitmapData);
            this.game.time.events.add(this.dayLight, function () {
                this.emitter.start(false, this.dayLight, 1, 0);
            }, this);
        };
        RainServiceProvider.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        RainServiceProvider.prototype.Stop = function () {
            if (this.emitter !== null && this.emitter.on == true) {
                this.emitter.on = false;
            }
        };
        return RainServiceProvider;
    }());
    TheElevatorPitchModule.RainServiceProvider = RainServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Staff = (function () {
        function Staff(currentDate) {
            this.CurrentDate = currentDate;
            this._initStats();
        }
        Staff.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        Staff.prototype._initStats = function () {
            this.initGender();
            this.initDOB();
            this.initName();
            this.initSenseOfHumor();
            this.initHealth();
            this.initStats();
        };
        Staff.prototype.initHealth = function () {
            this.Health = 10 - ((this.CurrentDate.getFullYear() - this.DOB.getFullYear()) / 10) | 0;
        };
        Staff.prototype.initSenseOfHumor = function () {
            this.SenseOfHumor = 10 - ((this.CurrentDate.getFullYear() - this.DOB.getFullYear()) / 10) | 0;
        };
        Staff.prototype.initGender = function () {
            this.Gender = this.intInRange(1, 2) == 1 ? TheElevatorPitchModule.eGender.Male : TheElevatorPitchModule.eGender.Female;
        };
        Staff.prototype.initName = function () {
            var self = this;
            //?gender=male
            $.ajax({
                url: 'https://randomuser.me/api',
                dataType: 'json',
                success: function (data) {
                    self.Name = self.capitalizeFirstLetter(data.results[0].name.first);
                },
                error: function () {
                    var namesM = ["Aaron",
                        "Abdul",
                        "dolph",
                        "Ahmed",
                        "Alejandro",
                        "Alfred",
                        "Alvaro",
                        "Anderson",
                        "Andy",
                        "Archie",
                        "Barney",
                        "Benny",
                        "Berry",
                        "Bob",
                        "Bradley",
                        "Brant",
                        "Buddy",
                        "Calvin",
                        "Carmine",
                        "Charlie",
                        "Chuck",
                        "Cole",
                        "Dave",
                        "Danny",
                        "Dee",
                        "Derrick",
                        "Dexter",
                        "Don",
                        "Doug",
                        "Earnest",
                        "Eddy",
                        "Elliot",
                        "Erasmo",
                        "Ezequiel",
                        "Felipe",
                        "Frank",
                        "Freddy",
                        "Fritz",
                        "George",
                        "Geoffrey",
                        "Gilbert",
                        "Glenn",
                        "Gonzalo",
                        "Gus",
                        "Harry",
                        "Hans",
                        "Homer",
                        "Herman",
                        "Herb",
                        "Miles",
                        "Murray",
                        "Monty",
                        "Pete",
                        "Pedro",
                        "Ralph",
                        "Rocco",
                        "Ron",
                        "Ted",
                        "Tod",
                        "Tim",
                        "Tom",
                        "Vito",
                        "Zachary"];
                    var namesF = [
                        "Adele",
                        "Angela",
                        "Audrey",
                        "Audrey",
                        "Barbra",
                        "Beckie",
                        "Berniece",
                        "Bethany",
                        "Beverley",
                        "Bonnie",
                        "Brenda",
                        "Britney",
                        "Brittney",
                        "Darleen",
                        "Dorothy",
                        "Dixie",
                        "Fannie",
                        "Florence",
                        "Grace",
                        "Gwen",
                        "Gwyneth",
                        "Gigi",
                        "Georgina",
                        "Hilary",
                        "Hollie",
                        "Hermine",
                        "Jacqueline",
                        "Janet",
                        "Jeane",
                        "Jennie",
                        "Josphine",
                        "Madeleine",
                        "Mandy",
                        "Melanie",
                        "Mickie",
                        "Pattie",
                        "Penny",
                        "Paulette",
                        "Rebbecca",
                        "Roxie",
                        "Roseline",
                        "Sandie",
                        "Sidney",
                        "Wilma",
                        "Wendie",
                        "Yvonne",
                        "Nicolette",
                        "Isabelle",
                        "Ivonne",
                        "Loreen"
                    ];
                    var currentNames = self.Gender == TheElevatorPitchModule.eGender.Male ? namesM : namesF;
                    self.Name = currentNames[self.intInRange(0, currentNames.length - 1)];
                }
            });
        };
        Staff.prototype.capitalizeFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        };
        Staff.prototype.initDOB = function () {
            var mm = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            var year = this.CurrentDate.getFullYear() - this.intInRange(18, 60);
            var m = this.intInRange(0, mm.length - 1);
            var month = m + 1;
            var day = this.intInRange(1, mm[m]);
            this.DOB = new Date(year, month, day);
        };
        Staff.prototype.getAge = function (currentDate) {
            return currentDate.getFullYear() - this.DOB.getFullYear();
        };
        Staff.prototype.WakeUp = function () {
        };
        Staff.prototype.Tac = function (hour) {
        };
        Staff.prototype.Update = function () {
        };
        return Staff;
    }());
    TheElevatorPitchModule.Staff = Staff;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    (function (eTypeStaff) {
        eTypeStaff[eTypeStaff["Programmer"] = 1] = "Programmer";
        eTypeStaff[eTypeStaff["ProjectManager"] = 2] = "ProjectManager";
        eTypeStaff[eTypeStaff["Tester"] = 3] = "Tester";
        eTypeStaff[eTypeStaff["BusinnessAnalist"] = 4] = "BusinnessAnalist";
        eTypeStaff[eTypeStaff["CTO"] = 5] = "CTO";
        eTypeStaff[eTypeStaff["CEO"] = 6] = "CEO";
    })(TheElevatorPitchModule.eTypeStaff || (TheElevatorPitchModule.eTypeStaff = {}));
    var eTypeStaff = TheElevatorPitchModule.eTypeStaff;
    (function (eGender) {
        eGender[eGender["Male"] = 1] = "Male";
        eGender[eGender["Female"] = 2] = "Female";
    })(TheElevatorPitchModule.eGender || (TheElevatorPitchModule.eGender = {}));
    var eGender = TheElevatorPitchModule.eGender;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var BusinnessAnalist = (function (_super) {
        __extends(BusinnessAnalist, _super);
        function BusinnessAnalist(currentDate) {
            _super.call(this, currentDate);
        }
        BusinnessAnalist.prototype.initStats = function () {
        };
        return BusinnessAnalist;
    }(TheElevatorPitchModule.Staff));
    TheElevatorPitchModule.BusinnessAnalist = BusinnessAnalist;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var CEO = (function (_super) {
        __extends(CEO, _super);
        function CEO(currentDate) {
            _super.call(this, currentDate);
        }
        CEO.prototype.initStats = function () {
        };
        return CEO;
    }(TheElevatorPitchModule.Staff));
    TheElevatorPitchModule.CEO = CEO;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var CTO = (function (_super) {
        __extends(CTO, _super);
        function CTO(currentDate) {
            _super.call(this, currentDate);
        }
        CTO.prototype.initStats = function () {
        };
        return CTO;
    }(TheElevatorPitchModule.Staff));
    TheElevatorPitchModule.CTO = CTO;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Programmer = (function (_super) {
        __extends(Programmer, _super);
        function Programmer(currentDate) {
            _super.call(this, currentDate);
        }
        Programmer.prototype.initStats = function () {
            var seed = 10 - Math.floor((this.CurrentDate.getFullYear() - this.DOB.getFullYear()) / 10);
            this.Debugging = this.intInRange(1, seed);
            this.ProblemSolving = this.intInRange(1, seed);
            this.Programming = this.intInRange(1, seed);
        };
        Programmer.prototype.Update = function () {
            _super.prototype.Update.call(this);
        };
        return Programmer;
    }(TheElevatorPitchModule.Staff));
    TheElevatorPitchModule.Programmer = Programmer;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var ProjectManager = (function (_super) {
        __extends(ProjectManager, _super);
        function ProjectManager(currentDate) {
            _super.call(this, currentDate);
        }
        ProjectManager.prototype.initStats = function () {
        };
        return ProjectManager;
    }(TheElevatorPitchModule.Staff));
    TheElevatorPitchModule.ProjectManager = ProjectManager;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Tester = (function (_super) {
        __extends(Tester, _super);
        function Tester(currentDate) {
            _super.call(this, currentDate);
        }
        Tester.prototype.initStats = function () {
        };
        return Tester;
    }(TheElevatorPitchModule.Staff));
    TheElevatorPitchModule.Tester = Tester;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var StaffFactory = (function () {
        function StaffFactory(currentDate, game) {
            this.currentDate = currentDate;
        }
        StaffFactory.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        StaffFactory.prototype.CreateStaff = function (type) {
            var result;
            switch (type) {
                case TheElevatorPitchModule.eTypeStaff.Programmer:
                    result = new TheElevatorPitchModule.Programmer(this.currentDate);
                    break;
                case TheElevatorPitchModule.eTypeStaff.ProjectManager:
                    result = new TheElevatorPitchModule.ProjectManager(this.currentDate);
                    break;
                case TheElevatorPitchModule.eTypeStaff.Tester:
                    result = new TheElevatorPitchModule.Tester(this.currentDate);
                    break;
                case TheElevatorPitchModule.eTypeStaff.BusinnessAnalist:
                    result = new TheElevatorPitchModule.BusinnessAnalist(this.currentDate);
                    break;
                case TheElevatorPitchModule.eTypeStaff.CTO:
                    result = new TheElevatorPitchModule.CTO(this.currentDate);
                    break;
                case TheElevatorPitchModule.eTypeStaff.CEO:
                    result = new TheElevatorPitchModule.CEO(this.currentDate);
                    break;
            }
            return result;
        };
        return StaffFactory;
    }());
    TheElevatorPitchModule.StaffFactory = StaffFactory;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var StaffServiceProvider = (function () {
        function StaffServiceProvider(game, utils, dayLight, currentDate) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
            this.currentDate = currentDate;
            this.staffFactory = new TheElevatorPitchModule.StaffFactory(currentDate, this.game);
        }
        StaffServiceProvider.prototype.Update = function (currentDate) {
            this.currentDate = currentDate;
        };
        StaffServiceProvider.prototype.CreateStaff = function (type) {
            return this.staffFactory.CreateStaff(type);
        };
        StaffServiceProvider.prototype.RemoveStaff = function (staff) {
            //staff.Destroy();// doto
        };
        return StaffServiceProvider;
    }());
    TheElevatorPitchModule.StaffServiceProvider = StaffServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var TheCity = (function (_super) {
        __extends(TheCity, _super);
        function TheCity() {
            _super.apply(this, arguments);
        }
        TheCity.prototype.preload = function () {
            this.fetchImages();
        };
        TheCity.prototype.create = function () {
            var dayLight = 5000;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.stage.backgroundColor = "#000";
            this.utils = new TheElevatorPitchModule.Utils(this.game);
            this.setBackgroundSprite();
            this.setDayAndNight();
            this.setSkyline();
            this.setLights();
            this.setSkyscraper();
            this.meteoServiceProvider = new TheElevatorPitchModule.MeteoServiceProvider(this.game, this.utils, new TheElevatorPitchModule.CloudServiceProvider(this.game, this.utils, dayLight), new TheElevatorPitchModule.RainServiceProvider(this.game, this.utils, dayLight), dayLight);
            this.trafficServiceProvider = new TheElevatorPitchModule.TrafficServiceProvider(this.game, this.utils, dayLight);
            this.dayNightServiceProvider = new TheElevatorPitchModule.DayNightServiceProvider(this.game, dayLight, this.utils, new TheElevatorPitchModule.DayNightServiceProviderOption(this.giveMeBackgroundSprites(), this.sunSprite, this.moonSprite, this.starsSprite, this.meteoServiceProvider), this.trafficServiceProvider);
            this.companyServiceProvider = new TheElevatorPitchModule.CompanyServiceProvider(this.game, this.utils, dayLight, this.dayNightServiceProvider.CurrentDate);
            this.companyServiceProvider.Create();
            this.registerEvents();
        };
        TheCity.prototype.update = function () {
            this.putOnTop();
            this.dayNightServiceProvider.Update();
        };
        TheCity.prototype.registerEvents = function () {
            var self = this;
            self.dayNightServiceProvider
                .SunRiseSignal.add(function (day) {
                self.companyServiceProvider.company.Staff
                    .forEach(function (s, idx, arr) {
                    s.WakeUp();
                });
            }, self);
            self.dayNightServiceProvider
                .ClockSignal.add(function (hour) {
                var h = hour;
                self.companyServiceProvider.company.Staff
                    .forEach(function (s, idx, arr) {
                    s.Tac(h);
                });
            }, self);
        };
        TheCity.prototype.fetchImages = function () {
            this.load.image("skyline", "Assets/images/skyline.png");
            this.load.image("skyscraper", "Assets/images/palace.png");
            this.load.image("officebackground", "Assets/images/officebackground.png");
            this.load.image("moon", "Assets/images/stars_mandala_lg.png");
            this.load.image("stars", "Assets/images/stars.png");
            this.load.image("sun", "Assets/images/sun-2.png");
            this.load.image("lights", "Assets/images/lights.png");
            this.load.image("cloud1", "Assets/images/cloud1.png");
            this.load.image("cloud2", "Assets/images/cloud2.png");
            this.load.image("cloud3", "Assets/images/cloud3.png");
            this.load.image("cloud4", "Assets/images/cloud4.png");
            this.load.image("cloud5", "Assets/images/cloud5.png");
            this.load.image("cloud6", "Assets/images/cloud6.png");
            this.load.image("car1", "Assets/images/car-1.png");
            this.load.image("car2", "Assets/images/car-2.png");
            this.load.image("car3", "Assets/images/car-3.png");
            this.load.image("car4", "Assets/images/car-4.png");
            this.load.image("autobus1", "Assets/images/autobus500.png");
        };
        TheCity.prototype.setDayAndNight = function () {
            this.sunSprite = this.game.add.sprite(0, this.game.height, "sun");
            var w = 120;
            this.sunSprite.width = w;
            this.sunSprite.height = w;
            this.moonSprite = this.game.add.sprite(this.game.width, this.game.height, "moon");
            this.moonSprite.width = w;
            this.moonSprite.height = w;
            this.starsSprite = this.game.add.sprite(this.game.width, this.game.height, "stars");
            this.starsSprite.width = w;
            this.starsSprite.height = w;
        };
        TheCity.prototype.setBackgroundSprite = function () {
            this.backgroundSprite = this.game.add.sprite(0, 0, this.giveMeBackground(this.game.width, this.game.height));
        };
        TheCity.prototype.setSkyscraper = function () {
            var skyscraper = this.game.cache.getImage("skyscraper");
            var officebackground = this.game.cache.getImage("officebackground");
            this.officebackground = this.game.add.tileSprite((this.game.width) - (skyscraper.width) - (skyscraper.width / 2), this.game.height - skyscraper.height, officebackground.width, officebackground.height, "officebackground");
            this.skyscraper = this.game.add.tileSprite((this.game.width) - (skyscraper.width) - (skyscraper.width / 2), this.game.height - skyscraper.height, skyscraper.width, skyscraper.height, "skyscraper");
            this.skyscraper.tint = 0x996600;
        };
        TheCity.prototype.setSkyline = function () {
            var skyline = this.game.cache.getImage("skyline");
            this.skyline = this.game.add.tileSprite(0, this.game.height - skyline.height, this.game.width, skyline.height, "skyline");
        };
        TheCity.prototype.setLights = function () {
            var lights = this.game.cache.getImage("lights");
            this.lights =
                this.game.add.tileSprite(0, this.game.height - lights.height, this.game.width, lights.height, "lights");
        };
        TheCity.prototype.giveMeBackground = function (width, height) {
            var bgBitmap = this.game.add.bitmapData(this.game.width, this.game.height);
            bgBitmap.ctx.rect(0, 0, this.game.width, this.game.height);
            bgBitmap.ctx.fillStyle = "#b2ddc8";
            bgBitmap.ctx.fill();
            return bgBitmap;
        };
        TheCity.prototype.giveMeBackgroundSprites = function () {
            var backgroundSprites = [
                { sprite: this.backgroundSprite, from: 0x1f2a27, to: 0xB2DDC8 },
                { sprite: this.skyline, from: 0x2f403b, to: 0x96CCBB },
                { sprite: this.lights, from: 0xffcc00, to: 0x00ffff }
            ];
            return backgroundSprites;
        };
        TheCity.prototype.putOnTop = function () {
            this.game.world.bringToTop(this.skyline);
            this.game.world.bringToTop(this.lights);
            this.game.world.bringToTop(this.officebackground);
            this.game.world.bringToTop(this.skyscraper);
            this.trafficServiceProvider.putOnTop();
            this.meteoServiceProvider.putOnTop();
        };
        return TheCity;
    }(Phaser.State));
    TheElevatorPitchModule.TheCity = TheCity;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var CarBuilder = (function () {
        function CarBuilder(game, utils, dayLenght, options) {
            this.game = game;
            this.dayLenght = dayLenght;
            this.options = options;
            this.utils = utils;
        }
        CarBuilder.prototype.Create = function () {
            var result = this.game.add.sprite(this.options.prevX, this.game.height - this.options.NextH, this.options.Selector);
            result.anchor.x = this.options.AnchorX;
            result.x = this.options.prevX;
            result.width = this.options.NextW;
            result.height = this.options.NextH;
            result.scale.x *= this.options.ScaleX;
            var limit = 256;
            if (!this.options.PutOnTop) {
                limit = 0;
            }
            var r = this.intInRange(0, limit);
            var g = this.intInRange(0, limit);
            var b = this.intInRange(0, limit);
            result.tint = this.RGBtoHEX(r, g, b);
            return result;
        };
        CarBuilder.prototype.RGBtoHEX = function (r, g, b) {
            return this.utils.RGBtoHEX(r, g, b);
        };
        CarBuilder.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        return CarBuilder;
    }());
    TheElevatorPitchModule.CarBuilder = CarBuilder;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var CarOptions = (function () {
        function CarOptions(prevX, Y, AnchorX, NextX, NextW, NextH, ScaleX, PutOnTop, Speed, Easing, Selector) {
            this.prevX = prevX;
            this.Y = Y;
            this.AnchorX = AnchorX;
            this.NextX = NextX;
            this.NextW = NextW;
            this.NextH = NextH;
            this.ScaleX = ScaleX;
            this.PutOnTop = PutOnTop;
            this.Speed = Speed;
            this.Easing = Easing;
            this.Selector = Selector;
        }
        return CarOptions;
    }());
    TheElevatorPitchModule.CarOptions = CarOptions;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var TrafficIntervall = (function () {
        function TrafficIntervall(condition, limit) {
            this.condition = condition;
            this.limit = limit;
        }
        return TrafficIntervall;
    }());
    TheElevatorPitchModule.TrafficIntervall = TrafficIntervall;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var TrafficServiceProvider = (function () {
        function TrafficServiceProvider(game, utils, dayLenght) {
            this.autobus = null;
            this.CurrentDate = 0;
            this.game = game;
            this.dayLenght = dayLenght;
            this.carSprites = [];
            this.utils = utils;
        }
        TrafficServiceProvider.prototype.spawn56 = function (date, hour) {
            if (hour === 8) {
                if (this.CurrentDate === undefined) {
                    this.CurrentDate = date.getDate();
                }
                if (this.CurrentDate !== date.getDate()) {
                    this.CurrentDate = date.getDate();
                    var autobus = this.game.cache.getImage("autobus1");
                    var isRightToLeft = true;
                    var perc = 50;
                    var nextW = (autobus.width * perc) / 100;
                    var nextH = (autobus.height * perc) / 100;
                    var prevX = 0 - nextW;
                    var nextX = this.game.width;
                    var velocity = this.dayLenght;
                    var easing = Phaser.Easing.Default;
                    if (isRightToLeft) {
                        prevX = this.game.width + nextW;
                        nextX = 0 - nextW;
                        easing = Phaser.Easing.Circular.InOut;
                    }
                    this.autobus = this.game.add.sprite(prevX, this.game.height - nextH, "autobus1");
                    this.autobus.width = nextW;
                    this.autobus.height = nextH;
                    this.autobus.anchor.set(isRightToLeft ? 1 : 0, this.autobus.anchor.y);
                    var autobusTween = this.game.add.tween(this.autobus).to({ x: nextX }, velocity, easing, true)
                        .onComplete.add(function (s, t) {
                        s.destroy();
                    });
                }
            }
        };
        TrafficServiceProvider.prototype.intInRange = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        TrafficServiceProvider.prototype.spawnTraffic = function (options) {
            var chance = this.intInRange(1, 1000) < 20;
            for (var i = 0; i < options.intervals.length; i++) {
                if (options.intervals[i].condition()) {
                    chance = this.intInRange(1, 1000) < options.intervals[i].limit;
                }
            }
            if (chance) {
                var isRightToLeft = this.intInRange(1, 2) === 2;
                var selector = "car" + this.intInRange(1, 4);
                var car = this.game.cache.getImage(selector);
                var nextH = options.putOnTop ? 40 : 30;
                var nextW = (car.width * nextH) / car.height;
                var prevX = 0;
                var nextX = this.game.width;
                var speed = this.dayLenght;
                var easing = Phaser.Easing.Default;
                var anchorX = 0;
                var scaleX = 1;
                if (isRightToLeft) {
                    prevX = this.game.width + nextW;
                    nextX = 0 - nextW;
                    anchorX = 1;
                }
                else {
                    prevX -= nextW;
                    nextX += nextW;
                    scaleX = -1;
                }
                var carSprite = new TheElevatorPitchModule.CarBuilder(this.game, this.utils, this.dayLenght, new TheElevatorPitchModule.CarOptions(prevX, this.game.height - nextH, anchorX, nextX, nextW, nextH, scaleX, options.putOnTop, speed, easing, selector)).Create();
                if (options.putOnTop) {
                    this.carSprites.push(carSprite);
                }
                // todo: speed should be ony multiply of 10 and not based of daylight(too slow)
                // todo: easing should be random from a pool of predefined: overridable
                var carTween = this.game.add.tween(carSprite).to({ x: nextX }, speed, easing, true)
                    .onComplete.add(function (s, t) {
                    s.destroy();
                });
            }
        };
        TrafficServiceProvider.prototype.Update = function (date, hour) {
            this.spawn56(date, hour);
            //let backTrafficOptions = new UpdateTrafficOptions(date, hour, false);
            //backTrafficOptions.intervals.push(new TrafficIntervall(() => true, 100));
            //this.spawnTraffic(backTrafficOptions);
            var trafficOptions = new TheElevatorPitchModule.UpdateTrafficOptions(date, hour, true);
            trafficOptions.intervals.push(new TheElevatorPitchModule.TrafficIntervall(function () { return (hour === 9) || (hour === 18); }, 150));
            this.spawnTraffic(trafficOptions);
        };
        TrafficServiceProvider.prototype.destroyAutobus = function (autobus) {
            autobus.destroy(true);
        };
        TrafficServiceProvider.prototype.putOnTop = function () {
            if (this.autobus !== null) {
                this.game.world.bringToTop(this.autobus);
            }
            for (var i = 0; i < this.carSprites.length; i++) {
                if (this.carSprites[i] != null) {
                    this.game.world.bringToTop(this.carSprites[i]);
                }
            }
        };
        return TrafficServiceProvider;
    }());
    TheElevatorPitchModule.TrafficServiceProvider = TrafficServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var UpdateTrafficOptions = (function () {
        function UpdateTrafficOptions(date, hour, putOnTop) {
            this.date = date;
            this.hour = hour;
            this.putOnTop = putOnTop;
            this.intervals = [];
        }
        return UpdateTrafficOptions;
    }());
    TheElevatorPitchModule.UpdateTrafficOptions = UpdateTrafficOptions;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Utils = (function () {
        function Utils(game) {
            this.game = game;
        }
        Utils.prototype.tweenTint = function (sprite, startColor, endColor, duration, easing) {
            var colorBlend = { step: 0 };
            this.game.add.tween(colorBlend).to({ step: 100 }, duration, easing ? easing : Phaser.Easing.Default, false)
                .onUpdateCallback(function () {
                sprite.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step, 1);
            })
                .start();
        };
        Utils.prototype.RGBtoHEX = function (r, g, b) {
            return r << 16 | g << 8 | b;
        };
        Utils.prototype.capitalizeFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        };
        return Utils;
    }());
    TheElevatorPitchModule.Utils = Utils;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
//# sourceMappingURL=app.js.map