﻿namespace TheElevatorPitchModule {
    "use strict";

    export class Boot extends Phaser.State {
        preload() {
        }
        create() {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;

            this.game.state.start("TheCity", true, false);

        }
    }
}