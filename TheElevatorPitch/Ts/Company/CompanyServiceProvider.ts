﻿namespace TheElevatorPitchModule {
    "use strict";

    export class CompanyServiceProvider {
        private president: Staff;
        private staffServiceProvider: StaffServiceProvider;
        private productBuilder: ProductLine.ProductServiceProvider;
        private game: Phaser.Game;
        private utils: Utils;
        private dayLight: number;
        private currentDate: Date;
        public company: Company;

        constructor(game : Phaser.Game, utils : Utils, dayLight : number, currentDate : Date) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
            this.currentDate = currentDate;
            this.productBuilder = new ProductLine.ProductServiceProvider();
            this.staffServiceProvider = new StaffServiceProvider(this.game, this.utils, this.dayLight, this.currentDate);
        }
         
        Create() {
            this.init();

        }

        Update() {
            this.putOnTop();
            this.company.Update(this.currentDate);

        }

        putOnTop() {

        }
        
        private init(): void {
            this.createCompany();
            this.createPresident();
            this.createStaff();
            this.addProducts();

            console.log(this.company);
        }

        private CalculateMontlyBalance(): void {
            this.company.CalculateMontlyBalance();
        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        } 

        private addProducts(): void {
            let options = new ProductLine.ProductOptions(<ProductLine.eTypeProduct>this.intInRange(1, ProductLine.eTypeProduct.SAAS));
            options.Version = new ProductLine.Version(<ProductLine.eTypePhase>this.intInRange(1, ProductLine.eTypePhase.Stable));
            this.company.addProduct(this.productBuilder.createProduct(options));
        }

        private createStaff() {
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(eTypeStaff.ProjectManager));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(eTypeStaff.BusinnessAnalist));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(eTypeStaff.Tester));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(eTypeStaff.Programmer));
            this.company.addStaffMember(this.staffServiceProvider.CreateStaff(eTypeStaff.Programmer));

        }

        private createCompany():void {
            this.company = new Company();
            this.company.Init();

        }

        private createPresident(): void {
            let self = this;
            let president = self.staffServiceProvider.CreateStaff(eTypeStaff.CEO);
            let gender = "male";
            if (president.Gender === eGender.Female) {
                gender = "female";
            }

            $.ajax({
                url: 'https://randomuser.me/api?gender=' + gender,
                dataType: 'json',
                success: function (data) {
                    president.Name = self.utils.capitalizeFirstLetter(data.results[0].name.first);
                    self.president = president;
                    self.company.Name = self.utils.capitalizeFirstLetter(data.results[0].name.last) + " Corporation";
                    self.company.Staff.push(president);
                },
                error: function () {
                    self.company.Name = "Evil Corporation";
                    self.company.Staff.push(president);

                }
            });

            
        }
    }
}