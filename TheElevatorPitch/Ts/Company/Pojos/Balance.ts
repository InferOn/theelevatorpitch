﻿namespace TheElevatorPitchModule {
    "use strict";

    export class Balance {
        public Value: number = 0;
        public Outcome: number = 0;
        public Income: number = 0;

    }
    
}