﻿namespace TheElevatorPitchModule {
    "use strict";

    export class Company {
        public Name: string;
        public Staff: Staff[];  
        public currentDate: Date;
        public Products: ProductLine.Product[];
        public balance: Balance;
        public Capital: number;

        constructor() {
            this.Name = "";
            this.Staff = [];
            this.currentDate = undefined;
            this.Products = [];
            this.balance = new Balance();
        }

        public CalculateMontlyBalance(): number {
            let expense = 0;
            this.Staff.forEach((element, index, staff) => {
                let tempExpense = element.Salary / 14;
                tempExpense += ((tempExpense * 40) / 100);
                expense += tempExpense;

            });

            this.Products.forEach((element, index, products) => {


            });

            return expense;
        }

        public Update(currentDate: Date) {
            this.currentDate = currentDate;
            this.Staff.forEach((staff: Staff, index: number, array: any) => {
                staff.Update();

            });
        }

        public addProduct(product: ProductLine.Product): void {
            this.Products.push(product);

        }

        public addStaffMember(member: Staff): void {
            this.Staff.push(member);

        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        } 

        public Init(): void {
            this.Capital = this.intInRange(1, 10) * 1000;

        }
    }
}