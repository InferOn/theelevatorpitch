﻿namespace TheElevatorPitchModule {
    "use strict";

    export namespace ProductLine {
        
        export class Version {
            public Major: number = 0;
            public Minor: number = 0;
            public Build: number = 0;

            constructor(typePhase: eTypePhase) {
                switch (typePhase) {
                    case eTypePhase.Alpha:
                        this.Minor = 1;
                        this.Build = this.intInRange(100, 1000);
                        break;

                    case eTypePhase.Beta:
                        this.Minor = 9;
                        break;

                    case eTypePhase.Stable:
                        this.Major = 1;
                        break;

                }
            }

            protected intInRange(min: number, max: number): number {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            } 

            public toString = (): string => {
                return this.Major + "." + this.Minor + "." + this.Build;
            }
        }
        
    }
}