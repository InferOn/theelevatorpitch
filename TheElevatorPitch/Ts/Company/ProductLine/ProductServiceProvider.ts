﻿namespace TheElevatorPitchModule {
    "use strict";

    export namespace ProductLine {
        
        export class ProductServiceProvider {

            public createProduct(options: ProductOptions): Product {
                let result: Product;
                switch (options.type) {
                    case eTypeProduct.BPM:
                        result = new BPM();
                        break;
                    case eTypeProduct.CMS:
                        result = new CMS();
                        break;
                    case eTypeProduct.CRM:
                        result = new CRM();
                        break;
                    case eTypeProduct.ORM:
                        result = new ORM();
                        break;
                    case eTypeProduct.SAAS:
                        result = new SAAS();
                        break;
                }

                this.applyVersion(result, options.Version);
                return result;
            }

            private applyVersion(product: Product, version: Version) {
                if (version != null && version != undefined) {
                    product.Version = version;

                } else {
                    product.Version = new Version(eTypePhase.Alpha);

                }
            }

            private upgradeProduct(product: Product, version: Version) {
                product.Upgrade(version);

            }
        }

    }
}