﻿namespace TheElevatorPitchModule {
    "use strict";

    export namespace ProductLine {

        export abstract class Product {
            public Name: string;
            public Version: Version
            public Customers: Customer[];

            constructor() {
                this.Customers = [];
            }

            public Upgrade(version: Version): void {
                this.Version = version;

            }
        }
        
        export class ProductOptions {
            public type: eTypeProduct;
            public Version : Version
            constructor(type: eTypeProduct) {
                this.type = type;
            }
        }
        
    }
}