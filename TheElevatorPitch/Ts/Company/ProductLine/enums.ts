﻿namespace TheElevatorPitchModule {
    "use strict";

    export namespace ProductLine {

        export enum eTypeProduct {
            CRM = 1,
            CMS = 2,
            BPM = 3,
            ORM = 4,
            SAAS = 5

        }
        
        export enum eTypePhase {
            Alpha = 1,
            Beta = 2,
            Stable = 3
        }
        
    }
}