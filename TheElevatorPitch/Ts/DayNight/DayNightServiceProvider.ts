﻿namespace TheElevatorPitchModule {
    "use strict";

    export class DayNightServiceProviderOption {
        public BackgroundSprites: any[];
        public SunSprite: Phaser.Sprite;
        public MoonSprite: Phaser.Sprite;
        public StarsSprite: Phaser.Sprite;

        public MeteoServiceProvider: MeteoServiceProvider;

        constructor(backgroundSprites: any[],
            sunSprite: Phaser.Sprite,
            moonSprite: Phaser.Sprite,
            starsSprite: Phaser.Sprite,
            meteoServiceProvider: MeteoServiceProvider
        ) {
            this.BackgroundSprites = backgroundSprites;
            this.SunSprite = sunSprite;
            this.MoonSprite = moonSprite;
            this.StarsSprite = starsSprite;
            this.MeteoServiceProvider = meteoServiceProvider;

        }
    }

    export class DayNightServiceProvider {
        private game: Phaser.Game;
        private dayLenght: number;
        private shading: any[];
        private sunSprite: Phaser.Sprite;
        private moonSprite: Phaser.Sprite;
        private starsSprite: Phaser.Sprite;
        private sunTween: Phaser.Tween;
        private moonTween: Phaser.Tween;
        private starTween: Phaser.Tween;
        private utils: Utils;
        private cloudServiceProvider: CloudServiceProvider;
        private meteoServiceProvider: MeteoServiceProvider;
        private trafficServiceProvider: TrafficServiceProvider;
        private Hour: number = 0;
        private TimeLapse: number;
        private ClockTime: Phaser.Text;
        private TimerEvent: Phaser.TimerEvent;
        private clockText: string;
        public CurrentDate: Date = new Date();
        public motionSpeed: number;
        public SunRiseSignal = new Phaser.Signal();
        public ClockSignal = new Phaser.Signal();

        constructor(game: Phaser.Game,
            dayLenght: number,
            utils: Utils,
            options: DayNightServiceProviderOption,
            trafficServiceProvider: TrafficServiceProvider) {
            this.game = game;
            this.utils = utils;
            this.dayLenght = dayLenght;
            this.shading = null;
            this.sunSprite = null;
            this.moonSprite = null;
            this.starsSprite = options.StarsSprite;
            this.TimeLapse = this.dayLenght / 12;
            this.motionSpeed = dayLenght + (dayLenght / 32);

            this.trafficServiceProvider = trafficServiceProvider;
            this.meteoServiceProvider = options.MeteoServiceProvider;

            this.initClockText();
            this.initShading(options.BackgroundSprites);
            this.initSun(options.SunSprite);
            this.initMoon(options.MoonSprite);
        }

        initClockText() {
            this.clockText = this.CurrentDate.toDateString() + " " + (this.Hour < 10 ? "0" + this.Hour : this.Hour + "") + ":00";
            this.ClockTime = this.game.add.text(10, 10, this.clockText, {});
            this.ClockTime.fontSize = 12;
            this.ClockTime.addColor("white", 0);

        }

        initSun(sprite: Phaser.Sprite) {
            this.sunSprite = sprite;
            this.sunRise(sprite);
        }

        initMoon(moon: Phaser.Sprite) {
            this.moonSprite = moon;
            this.moonSet(moon);
        }

        sunRise(sprite: Phaser.Sprite) {
            this.CurrentDate.setDate(this.CurrentDate.getDate() + 1);
            this.SunRiseSignal.dispatch(this.CurrentDate.getDate());

            sprite.position.x = this.game.width - sprite.width - (sprite.width / 2);
            this.sunTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.Out, true);
            this.utils.tweenTint(sprite, 0xff6600, 0xffff00, this.motionSpeed, Phaser.Easing.Default);
            this.sunTween.onComplete.add(this.sunSet, this);

            if (this.TimerEvent == null) {
                this.TimerEvent = this.game.time.events.loop(this.TimeLapse, () => {
                    if (this.Hour === 24) {
                        this.Hour = 0;
                    } else {
                        this.Hour++;
                        this.ClockSignal.dispatch(this.Hour);

                    }
                    this.clockText = (this.Hour < 10 ? "0" + this.Hour : this.Hour + "") + ":00";
                    this.ClockTime.text = this.CurrentDate.toDateString() + " " + this.clockText;
                });
            }
            if (this.shading) {
                this.shading.forEach((sprite: any) => {
                    this.utils.tweenTint(sprite.sprite, sprite.from, sprite.to, this.motionSpeed, Phaser.Easing.Linear.None);

                });
            }
        }

        sunSet(sprite: Phaser.Sprite) {
            sprite.position.x = sprite.width + (sprite.width / 2);
            this.sunTween = this.game.add.tween(sprite)
                .to({ y: this.game.world.height + (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.In, true);
            this.utils.tweenTint(sprite, 0xffff00, 0xff6600, this.dayLenght, Phaser.Easing.Circular.In);
            this.sunTween.onComplete.add(this.sunRise, this);
            if (this.shading) {
                this.shading.forEach((sprite: any) => {
                    this.utils.tweenTint(sprite.sprite, sprite.to, sprite.from, this.motionSpeed, Phaser.Easing.Circular.In);
                });
            }
        }

        moonRise(sprite: Phaser.Sprite) {
            sprite.position.x = this.game.width - sprite.width - (sprite.width / 2);
            this.moonTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.Out, true);
            this.moonTween.onComplete.add(this.moonSet, this);

        }

        moonSet(moon: Phaser.Sprite) {
            moon.position.x = moon.width + (moon.width / 2);
            this.moonTween = this.game.add.tween(moon)
                .to({ y: this.game.world.height + (moon.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.In, true);
            this.moonTween.onComplete.add(this.moonRise, this);

        }

        initShading(sprites: any[]) {
            this.shading = sprites;
        }

        Update() {
            this.trafficServiceProvider.Update(this.CurrentDate, this.Hour);
            this.meteoServiceProvider.Update(this.CurrentDate, this.Hour);

        }
    }
}