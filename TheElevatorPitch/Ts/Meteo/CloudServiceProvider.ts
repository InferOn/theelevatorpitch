﻿namespace TheElevatorPitchModule {
    "use strict";

    export class CloudServiceProvider {
        private game: Phaser.Game;
        private utils: Utils;
        private meteoContitions: MeteoResult;
        private dayLight: number;
        private clouds: Phaser.Sprite[];

        constructor(game: Phaser.Game, utils: Utils, dayLight: number) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
            this.clouds = [];
        }

        Update(meteoContitions: MeteoResult) {
            this.meteoContitions = meteoContitions;
            if (this.meteoContitions.spawnCloud) {
                let selector = "cloud" + this.intInRange(1, 6);
                let cloud = this.game.cache.getImage(selector);

                let isRightToLeft = this.intInRange(1, 2) === 2;
                let perc = this.intInRange(20, 40);
                let nextW = (cloud.width * perc) / 100;
                let nextH = (cloud.height * perc) / 100;
                let prevX = 0 - nextW - 50;
                let nextX = this.game.width + 50;
                if (isRightToLeft) {
                    prevX = this.game.width + nextW;
                    nextX = 0 - nextW;
                }

                let sprite = this.game.add.sprite(prevX, this.intInRange(-10, (this.game.height / 2) ), selector);
                this.clouds.push(sprite);

                sprite.width = nextW;
                sprite.height = nextH;
                sprite.alpha = this.intInRange(80, 90);
                sprite.anchor.set(isRightToLeft ? 1 : 0, sprite.anchor.y);

                let startTint = this.utils.RGBtoHEX(255, 255, 255);
                sprite.tint = startTint;
                let endTint = this.utils.RGBtoHEX(0, 0, 0);

                let speed = this.intInRange(this.dayLight * 4, this.dayLight * 4);
                let cloudSpeedEasing = Phaser.Easing.Linear.None;
                let cloudTintEasing = Phaser.Easing.Default;

                if (this.meteoContitions.cloudSpeed > 0) {
                    speed = this.meteoContitions.cloudSpeed;
                    cloudSpeedEasing = Phaser.Easing.Linear.None;
                    cloudTintEasing = Phaser.Easing.Exponential.Out;

                }
                if (this.meteoContitions.tint) {
                    this.utils.tweenTint(sprite, startTint, endTint, speed, cloudTintEasing);

                }

                this.game.add.tween(sprite).to({ x: nextX }, speed, cloudSpeedEasing, true)
                    .onComplete.add((s: any, t: any) => {
                        s.destroy();
                    });
            }
        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
}