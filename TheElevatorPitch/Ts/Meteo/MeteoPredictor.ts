﻿namespace TheElevatorPitchModule {
    "use strict";

    export class MeteoPredictor {
        private seasons: any[];
        private game: Phaser.Game;
        private dateDay: number = undefined;

        private duration: number = 0;
        private day: number = 0;
        private condition: eMeteoConditions;
        private spawnCloud: boolean;
        private cloudSpeed: number;
        private tint: boolean;

        constructor(game: Phaser.Game) {
            this.game = game;
            this.seasons = [];
            this.seasons.push(new SeasonInfo("Spring", 3));
            this.seasons.push(new SeasonInfo("Summer", 6));
            this.seasons.push(new SeasonInfo("Autumn", 9));
            this.seasons.push(new SeasonInfo("Winter", 12));
        }

        public Predict(date: Date, hour: number): MeteoResult {
            let result = new MeteoResult();
            let start = false;
            let currentDay = date.getDate();

            if (this.dateDay === undefined) {
                this.dateDay = currentDay;
                this.setCondition();
                result.shouldUpdate = true;
                start = true;
            }

            if (this.dateDay !== currentDay) {
                this.day++;
                this.dateDay = currentDay;
                if (this.duration === this.day) {
                    this.day = 0;
                    this.setCondition();
                    result.shouldUpdate = true;

                }

            } else {
                if (!start)
                    result.shouldUpdate = false;

            }

            result.conditions = this.condition;
            this.setClouds();
            result.cloudSpeed = this.cloudSpeed;
            result.spawnCloud = this.spawnCloud;
            result.tint = this.tint;

            return result;
        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        setCondition() {

            this.duration = this.intInRange(1, this.intInRange(3, 10));

            let prob = this.intInRange(1, 100);

            if (prob <= 10) {
                this.condition = eMeteoConditions.Sunny;

            } else if (prob > 10 && prob <= 40) {
                this.condition = eMeteoConditions.ModeratelyCloudly;

            } else if (prob > 40 && prob <= 60) {
                this.condition = eMeteoConditions.Cloudly;

            } else if (prob > 60) {
                this.condition = eMeteoConditions.Shitty;

            }

            // shitty mode
            //this.duration = 1;
            //this.condition = eMeteoConditions.Shitty;

        }

        setClouds() {
            this.tint = false;
            if (this.condition == eMeteoConditions.Sunny) {
                this.spawnCloud = this.intInRange(1, 10000) < 10;

            } else if (this.condition == eMeteoConditions.ModeratelyCloudly) {
                this.spawnCloud = this.intInRange(1, 10000) < 100;

            }
            else if (this.condition == eMeteoConditions.Cloudly) {
                this.spawnCloud = this.intInRange(1, 10000) < 500;

            }
            else if (this.condition == eMeteoConditions.Shitty) {
                this.spawnCloud = this.intInRange(1, 10000) < 8000;
                this.cloudSpeed = 10000;
                this.tint = true;

            } 
            else if (this.condition == eMeteoConditions.Snow) {
                this.spawnCloud = this.intInRange(1, 100000) < 10;

            }
        }
    }
}