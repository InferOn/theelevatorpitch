﻿namespace TheElevatorPitchModule {
    "use strict";

    export class MeteoServiceProvider {
        private game: Phaser.Game;
        private utils: Utils;
        private currentCondition: eMeteoConditions;
        private meteoPredictor: MeteoPredictor;
        private dayLight: number;
        public CloudServiceProvider: CloudServiceProvider;
        public RainServiceProvider: RainServiceProvider;
        private MeteoText: Phaser.Text;


        constructor(game: Phaser.Game, utils: Utils, cloudServiceProvider: CloudServiceProvider, rainServiceProvider: RainServiceProvider, dayLight: number) {
            this.dayLight = dayLight;
            this.game = game;
            this.utils = utils;
            this.CloudServiceProvider = cloudServiceProvider;
            this.RainServiceProvider = rainServiceProvider;
            this.meteoPredictor = new MeteoPredictor(game);
            this.initMeteoText();

        }

        initMeteoText() {
            this.MeteoText = this.game.add.text(10, 30, "Meteo: /", {});;
            this.MeteoText.fontSize = 12;
            this.MeteoText.addColor("white", 0);

        }
        Update(date: Date, hour: number) {
            let result = this.meteoPredictor.Predict(date, hour);
            let meteoMsg = ["Sunny", "Moderately Cloudly", "Cloudly", "Shitty", "Snow"];
            this.MeteoText.text = "Meteo: " + meteoMsg[result.conditions];
            this.CloudServiceProvider.Update(result);
            if (result.shouldUpdate) {
                this.RainServiceProvider.Update(result);

            }
        }

        putOnTop() {
            this.RainServiceProvider.putOnTop();
        }
    }

}