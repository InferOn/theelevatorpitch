﻿namespace TheElevatorPitchModule {
    "use strict";

    export class RainServiceProvider {
        private game: Phaser.Game;
        private utils: Utils;
        private emitter: Phaser.Particles.Arcade.Emitter = null;
        private bitmapData: Phaser.BitmapData = null;
        private dayLight: number;
        constructor(game: Phaser.Game, utils: Utils, dayLight: number) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
        }
        public Update(meteoContitions: MeteoResult) {
            if (meteoContitions.conditions === eMeteoConditions.Shitty) {
                this.Start();
            }
            else {
                this.Stop();

            }
        }

        public putOnTop() {
            if (this.emitter != null) {
                this.game.world.bringToTop(this.emitter);

            }
        }

        private addTear(bitmapData, rainLenght, x, y) {
            bitmapData.ctx.rect(x, y, 5, rainLenght);
            bitmapData.ctx.fillStyle = "#9cc9de";
            bitmapData.ctx.fill();

        }

        public Start() {
            let rainLenght = 2000;
            if (this.emitter == null) {
                this.emitter = this.game.add.emitter(this.game.world.centerX, 0, 1500);
                this.bitmapData = this.game.add.bitmapData(this.game.world.width, rainLenght + 50);

            }
            this.emitter.width = this.game.world.width + (this.game.world.width / 2);

            if (this.emitter.on == false) {
                this.addTear(this.bitmapData, rainLenght, 0, 0);
                var p = 0;
                var constant = 200;

                for (let i = constant; i < this.emitter.width; i += constant) {
                    this.addTear(this.bitmapData, rainLenght, i, 0);
                }

                this.emitter.width = this.game.world.width + (this.game.world.width / 2);
                this.emitter.minParticleScale = 0.1;
                this.emitter.maxParticleScale = 0.1;
                this.emitter.setYSpeed(2000, 3000);
                var dir = this.intInRange(1, 2);

                var minAngle, maxAngle;
                if (dir == 1) {
                    minAngle = -20;
                    maxAngle = -10;
                    this.emitter.pivot.x = 0;

                }
                else {
                    minAngle = 10;
                    maxAngle = 20;
                    this.emitter.pivot.x = 1;

                }

                this.emitter.angle = this.intInRange(minAngle, maxAngle);
                this.emitter.minRotation = -20;
                this.emitter.maxRotation = 20;

            }

            this.emitter.makeParticles(this.bitmapData);
            this.game.time.events.add(this.dayLight, function () {
                this.emitter.start(false, this.dayLight, 1, 0);

            }, this);


        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        public Stop() {
            if (this.emitter !== null && this.emitter.on == true) {
                this.emitter.on = false;
            }
        }
    }
}
