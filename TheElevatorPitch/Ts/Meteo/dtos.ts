﻿namespace TheElevatorPitchModule {
    "use strict";

    export class MeteoResult {
        public conditions: eMeteoConditions;
        public spawnCloud: boolean;
        public shouldUpdate: boolean;
        public tint: boolean;
        public cloudSpeed: number = 0;
        constructor() {
            this.shouldUpdate = true;
        }
    }

    export class SeasonInfo {
        public Name: string;
        public StartMonth: number;
        constructor(name: string, startMonth: number) {
            this.Name = name;
            this.StartMonth = startMonth;
        }
    }
}