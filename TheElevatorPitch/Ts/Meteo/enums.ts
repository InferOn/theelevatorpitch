﻿namespace TheElevatorPitchModule {
    "use strict";

    export enum eMeteoConditions {
        Sunny = 0,
        ModeratelyCloudly = 1,
        Cloudly = 2,
        Shitty = 3,
        Snow = 4 // tbd

    }
}