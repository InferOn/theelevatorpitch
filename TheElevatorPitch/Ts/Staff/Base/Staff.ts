﻿namespace TheElevatorPitchModule {
    "use strict";

    export interface IUpdatable {
        Update(): void;
    }

    export abstract class Staff implements IUpdatable {
        public DOB: Date;
        public Health: number;
        public Sprite: Phaser.Sprite;
        public Name: string;
        public SenseOfHumor: number;
        public Gender: eGender;
        public Salary: number;
        protected CurrentDate: Date;

        constructor(currentDate: Date) {
            this.CurrentDate = currentDate;
            this._initStats();

        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        private _initStats() {
            this.initGender();
            this.initDOB();
            this.initName();
            this.initSenseOfHumor();
            this.initHealth();
            this.initStats();
        }

        private initHealth() {
            this.Health = 10 - ((this.CurrentDate.getFullYear() - this.DOB.getFullYear()) / 10) | 0;
        }

        private initSenseOfHumor() {
            this.SenseOfHumor = 10 - ((this.CurrentDate.getFullYear() - this.DOB.getFullYear()) / 10) | 0;
        }

        private initGender() {
            this.Gender = this.intInRange(1, 2) == 1 ? eGender.Male : eGender.Female;

        }

        private initName() {
            let self = this;
            //?gender=male
            $.ajax({
                url: 'https://randomuser.me/api',
                dataType: 'json',
                success: function (data) {
                    self.Name = self.capitalizeFirstLetter(data.results[0].name.first);

                }, 
                error: function () {
                    let namesM = ["Aaron",
                        "Abdul",
                        "dolph",
                        "Ahmed",
                        "Alejandro",
                        "Alfred",
                        "Alvaro",
                        "Anderson",
                        "Andy",
                        "Archie",
                        "Barney",
                        "Benny",
                        "Berry",
                        "Bob",
                        "Bradley",
                        "Brant",
                        "Buddy",
                        "Calvin",
                        "Carmine",
                        "Charlie",
                        "Chuck",
                        "Cole",
                        "Dave",
                        "Danny",
                        "Dee",
                        "Derrick",
                        "Dexter",
                        "Don",
                        "Doug",
                        "Earnest",
                        "Eddy",
                        "Elliot",
                        "Erasmo",
                        "Ezequiel",
                        "Felipe",
                        "Frank",
                        "Freddy",
                        "Fritz",
                        "George",
                        "Geoffrey",
                        "Gilbert",
                        "Glenn",
                        "Gonzalo",
                        "Gus",
                        "Harry",
                        "Hans",
                        "Homer",
                        "Herman",
                        "Herb",
                        "Miles",
                        "Murray",
                        "Monty",
                        "Pete",
                        "Pedro",
                        "Ralph",
                        "Rocco",
                        "Ron",
                        "Ted",
                        "Tod",
                        "Tim",
                        "Tom",
                        "Vito",
                        "Zachary"];
                    let namesF = [
                        "Adele",
                        "Angela",
                        "Audrey",
                        "Audrey",
                        "Barbra",
                        "Beckie",
                        "Berniece",
                        "Bethany",
                        "Beverley",
                        "Bonnie",
                        "Brenda",
                        "Britney",
                        "Brittney",
                        "Darleen",
                        "Dorothy",
                        "Dixie",
                        "Fannie",
                        "Florence",
                        "Grace",
                        "Gwen",
                        "Gwyneth",
                        "Gigi",
                        "Georgina",
                        "Hilary",
                        "Hollie",
                        "Hermine",
                        "Jacqueline",
                        "Janet",
                        "Jeane",
                        "Jennie",
                        "Josphine",
                        "Madeleine",
                        "Mandy",
                        "Melanie",
                        "Mickie",
                        "Pattie",
                        "Penny",
                        "Paulette",
                        "Rebbecca",
                        "Roxie",
                        "Roseline",
                        "Sandie",
                        "Sidney",
                        "Wilma",
                        "Wendie",
                        "Yvonne",
                        "Nicolette",
                        "Isabelle",
                        "Ivonne",
                        "Loreen"
                    ];
                    let currentNames = self.Gender == eGender.Male ? namesM : namesF;
                    self.Name = currentNames[self.intInRange(0, currentNames.length - 1)];
                }
            });

        }

        protected capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        private initDOB() {
            let mm = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            let year = this.CurrentDate.getFullYear() - this.intInRange(18, 60);

            let m = this.intInRange(0, mm.length-1);
            let month = m + 1;
            let day = this.intInRange(1, mm[m]);

            this.DOB = new Date(year, month, day);

        }

        public getAge(currentDate: Date): number {
            return currentDate.getFullYear() - this.DOB.getFullYear();
        }

        protected abstract initStats(): void;

        public WakeUp(): void {

        }

        public Tac(hour: number): void {

        }

        public Update() {

        }
    }
}