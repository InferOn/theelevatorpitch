﻿namespace TheElevatorPitchModule {
    "use strict";
    
    export class Programmer extends Staff implements IUpdatable{
        private ProblemSolving: number;
        private Programming: number;
        private Debugging: number;

        constructor(currentDate: Date) {
            super(currentDate);
        }

        protected initStats(): void {
            let seed = 10 - Math.floor((this.CurrentDate.getFullYear() - this.DOB.getFullYear()) / 10);
            this.Debugging = this.intInRange(1, seed);
            this.ProblemSolving = this.intInRange(1, seed);
            this.Programming = this.intInRange(1, seed);
        }

        public Update() {
            super.Update();

        }

    }
    
}