﻿namespace TheElevatorPitchModule {
    "use strict";

    export class StaffFactory {
        private currentDate: Date;

        constructor(currentDate: Date, game: Phaser.Game) {
            this.currentDate = currentDate;
        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        public CreateStaff(type: eTypeStaff): Staff {
            let result: Staff;

            switch (type) {
                case eTypeStaff.Programmer:
                    result = new Programmer(this.currentDate);
                    break;

                case eTypeStaff.ProjectManager:
                    result = new ProjectManager(this.currentDate);
                    break;

                case eTypeStaff.Tester:
                    result = new Tester(this.currentDate);
                    break;

                case eTypeStaff.BusinnessAnalist:
                    result = new BusinnessAnalist(this.currentDate);
                    break;

                case eTypeStaff.CTO:
                    result = new CTO(this.currentDate);
                    break;

                case eTypeStaff.CEO:
                    result = new CEO(this.currentDate);
                    break;

            }

            return result;
        }
    }
}