﻿namespace TheElevatorPitchModule {
    "use strict";

    export class StaffServiceProvider{
        private game: Phaser.Game;
        private utils: Utils;
        private dayLight: number;
        private currentDate: Date;
        private staffFactory: StaffFactory;

        constructor(game: Phaser.Game, utils: Utils, dayLight: number, currentDate: Date) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
            this.currentDate = currentDate;
            this.staffFactory = new StaffFactory(currentDate, this.game);
        }
        
        public Update(currentDate: Date) {
            this.currentDate = currentDate;
            
        }

        public CreateStaff(type: eTypeStaff): Staff {
            return this.staffFactory.CreateStaff(type);

        }
        public RemoveStaff(staff: Staff): void {
            //staff.Destroy();// doto

        }
        
    }

}