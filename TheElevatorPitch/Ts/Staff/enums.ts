﻿namespace TheElevatorPitchModule {
    "use strict";

    export enum eTypeStaff {
        Programmer = 1,
        ProjectManager = 2,
        Tester = 3,
        BusinnessAnalist = 4,
        CTO = 5,
        CEO = 6

    }

    export enum eGender {
        Male = 1,
        Female = 2
    }

}