﻿namespace TheElevatorPitchModule {
    "use strict";

    export class TheCity extends Phaser.State {
        private backgroundSprite: Phaser.Sprite;
        private sunSprite: Phaser.Sprite;
        private moonSprite: Phaser.Sprite;
        private starsSprite: Phaser.Sprite;
        private skyline: Phaser.TileSprite;
        private skyscraper: Phaser.TileSprite;
        private officebackground: Phaser.TileSprite;
        private lights: Phaser.TileSprite;
        private utils: Utils;
        private meteoCondition: MeteoResult;
        private dayNightServiceProvider: DayNightServiceProvider;
        private meteoServiceProvider: MeteoServiceProvider;
        private trafficServiceProvider: TrafficServiceProvider;
        private companyServiceProvider: CompanyServiceProvider;

        preload() {
            this.fetchImages();

        }

        create() {
            let dayLight = 5000;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.stage.backgroundColor = "#000";

            this.utils = new Utils(this.game);

            this.setBackgroundSprite();
            this.setDayAndNight();
            this.setSkyline();
            this.setLights();
            this.setSkyscraper();

            this.meteoServiceProvider = new MeteoServiceProvider(this.game,
                this.utils,
                new CloudServiceProvider(this.game, this.utils, dayLight),
                new RainServiceProvider(this.game, this.utils, dayLight), dayLight);

            this.trafficServiceProvider = new TrafficServiceProvider(this.game, this.utils, dayLight);

            this.dayNightServiceProvider = new DayNightServiceProvider(this.game,
                dayLight,
                this.utils,
                new DayNightServiceProviderOption(this.giveMeBackgroundSprites(),
                    this.sunSprite,
                    this.moonSprite,
                    this.starsSprite,
                    this.meteoServiceProvider),
                this.trafficServiceProvider);

            this.companyServiceProvider = new CompanyServiceProvider(this.game, this.utils, dayLight, this.dayNightServiceProvider.CurrentDate);
            this.companyServiceProvider.Create();

            this.registerEvents();

        }

        update() {
            this.putOnTop();
            this.dayNightServiceProvider.Update();
        }

        private registerEvents() {
            let self = this;

            self.dayNightServiceProvider
                .SunRiseSignal.add(function (day) {

                    self.companyServiceProvider.company.Staff
                        .forEach(function (s, idx, arr) {
                            s.WakeUp();
                        });
                }, self);

            self.dayNightServiceProvider
                .ClockSignal.add(function (hour) {
                    let h = hour;
                    self.companyServiceProvider.company.Staff
                        .forEach(function (s, idx, arr) {
                            s.Tac(h);
                        });
                }, self);
        }

        private fetchImages() {
            this.load.image("skyline", "Assets/images/skyline.png");
            this.load.image("skyscraper", "Assets/images/palace.png");
            this.load.image("officebackground", "Assets/images/officebackground.png");
            this.load.image("moon", "Assets/images/stars_mandala_lg.png");
            this.load.image("stars", "Assets/images/stars.png");
            this.load.image("sun", "Assets/images/sun-2.png");
            this.load.image("lights", "Assets/images/lights.png");
            this.load.image("cloud1", "Assets/images/cloud1.png");
            this.load.image("cloud2", "Assets/images/cloud2.png");
            this.load.image("cloud3", "Assets/images/cloud3.png");
            this.load.image("cloud4", "Assets/images/cloud4.png");
            this.load.image("cloud5", "Assets/images/cloud5.png");
            this.load.image("cloud6", "Assets/images/cloud6.png");
            this.load.image("car1", "Assets/images/car-1.png");
            this.load.image("car2", "Assets/images/car-2.png");
            this.load.image("car3", "Assets/images/car-3.png");
            this.load.image("car4", "Assets/images/car-4.png");
            this.load.image("autobus1", "Assets/images/autobus500.png");
        }

        private setDayAndNight() {
            this.sunSprite = this.game.add.sprite(0, this.game.height, "sun");
            let w = 120;
            this.sunSprite.width = w;
            this.sunSprite.height = w;


            this.moonSprite = this.game.add.sprite(this.game.width, this.game.height, "moon");
            this.moonSprite.width = w;
            this.moonSprite.height = w;


            this.starsSprite = this.game.add.sprite(this.game.width, this.game.height, "stars");
            this.starsSprite.width = w;
            this.starsSprite.height = w;

        }

        private setBackgroundSprite() {
            this.backgroundSprite = this.game.add.sprite(0, 0, this.giveMeBackground(this.game.width, this.game.height));

        }

        private setSkyscraper() {
            var skyscraper = this.game.cache.getImage("skyscraper");
            var officebackground = this.game.cache.getImage("officebackground");

            this.officebackground = this.game.add.tileSprite(
                (this.game.width) - (skyscraper.width) - (skyscraper.width / 2),
                this.game.height - skyscraper.height,
                officebackground.width,
                officebackground.height, "officebackground"
            );

            this.skyscraper = this.game.add.tileSprite(
                (this.game.width) - (skyscraper.width) - (skyscraper.width / 2),
                this.game.height - skyscraper.height,
                skyscraper.width,
                skyscraper.height, "skyscraper"
            );
            this.skyscraper.tint = 0x996600;
        }

        private setSkyline() {
            var skyline = this.game.cache.getImage("skyline");
            this.skyline = this.game.add.tileSprite(0,
                this.game.height - skyline.height,
                this.game.width,
                skyline.height, "skyline"
            );
        }

        private setLights() {
            var lights = this.game.cache.getImage("lights");
            this.lights =
                this.game.add.tileSprite(0,
                    this.game.height - lights.height,
                    this.game.width,
                    lights.height, "lights"
                );
        }

        private giveMeBackground(width: number, height: number) {
            let bgBitmap = this.game.add.bitmapData(this.game.width, this.game.height);
            bgBitmap.ctx.rect(0, 0, this.game.width, this.game.height);
            bgBitmap.ctx.fillStyle = "#b2ddc8";
            bgBitmap.ctx.fill();
            return bgBitmap;
        }

        private giveMeBackgroundSprites() {
            let backgroundSprites = [
                { sprite: this.backgroundSprite, from: 0x1f2a27, to: 0xB2DDC8 },
                { sprite: this.skyline, from: 0x2f403b, to: 0x96CCBB },
                { sprite: this.lights, from: 0xffcc00, to: 0x00ffff }
            ];

            return backgroundSprites;
        }

        private putOnTop() {
            this.game.world.bringToTop(this.skyline);
            this.game.world.bringToTop(this.lights);
            this.game.world.bringToTop(this.officebackground);
            this.game.world.bringToTop(this.skyscraper);

            this.trafficServiceProvider.putOnTop();
            this.meteoServiceProvider.putOnTop();

        }
    }
}