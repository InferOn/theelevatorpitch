﻿namespace TheElevatorPitchModule {
    "use strict";

    export class CarBuilder {
        game: Phaser.Game;
        dayLenght: number;
        options: CarOptions;
        utils: Utils;
        constructor(game: Phaser.Game, utils: Utils, dayLenght: number, options: CarOptions) {
            this.game = game;
            this.dayLenght = dayLenght;
            this.options = options;
            this.utils = utils;

        }

        public Create(): Phaser.Sprite {
            let result = this.game.add.sprite(this.options.prevX, this.game.height - this.options.NextH, this.options.Selector);
            result.anchor.x = this.options.AnchorX;
            result.x = this.options.prevX;
            result.width = this.options.NextW;
            result.height = this.options.NextH;
            result.scale.x *= this.options.ScaleX;

            let limit = 256;
            if (!this.options.PutOnTop) { limit = 0; }

            let r = this.intInRange(0, limit);
            let g = this.intInRange(0, limit);
            let b = this.intInRange(0, limit);

            result.tint = this.RGBtoHEX(r, g, b);
            return result;
        }

        private RGBtoHEX(r: number, g: number, b: number) {
            return this.utils.RGBtoHEX(r, g, b);
        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }

}