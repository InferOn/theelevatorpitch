﻿namespace TheElevatorPitchModule {
    "use strict";

    export class CarOptions {
        prevX: number;
        Y: number;
        AnchorX: number;
        NextX: number;
        NextW: number;
        NextH: number;
        ScaleX: number;
        PutOnTop: boolean;
        Speed: number;
        Easing: any;
        Selector: string;

        constructor(prevX: number, Y: number, AnchorX: number, NextX: number, NextW: number, NextH: number, ScaleX: number,
            PutOnTop: boolean,
            Speed: number,
            Easing: any,
            Selector: string
        ) {
            this.prevX = prevX;
            this.Y = Y;
            this.AnchorX = AnchorX;
            this.NextX = NextX;
            this.NextW = NextW;
            this.NextH = NextH;
            this.ScaleX = ScaleX;
            this.PutOnTop = PutOnTop;
            this.Speed = Speed;
            this.Easing = Easing;
            this.Selector = Selector;

        }
    }
    
}