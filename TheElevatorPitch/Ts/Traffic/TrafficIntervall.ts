﻿namespace TheElevatorPitchModule {
    "use strict";
    
    export class TrafficIntervall {
        public limit: number;
        public condition: () => boolean;

        constructor(condition: () => boolean, limit: number) {
            this.condition = condition;
            this.limit = limit;
        }
    }
}