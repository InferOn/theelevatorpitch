﻿namespace TheElevatorPitchModule {
    "use strict";

    export class TrafficServiceProvider {
        private game: Phaser.Game;
        private utils: Utils;
        private dayLenght: number;
        private carSprites: Phaser.Sprite[];
        private autobus: Phaser.Sprite = null;
        private autobusTween: Phaser.Tween;
        private CurrentDate: number = 0;

        constructor(game: Phaser.Game, utils: Utils, dayLenght: number) {
            this.game = game;
            this.dayLenght = dayLenght;
            this.carSprites = [];
            this.utils = utils;
        }

        private spawn56(date: Date, hour: number) {
            if (hour === 8) {
                if (this.CurrentDate === undefined) {
                    this.CurrentDate = date.getDate();
                }

                if (this.CurrentDate !== date.getDate()) {
                    this.CurrentDate = date.getDate();
                    let autobus = this.game.cache.getImage("autobus1");

                    let isRightToLeft = true;
                    let perc = 50;
                    let nextW = (autobus.width * perc) / 100;
                    let nextH = (autobus.height * perc) / 100;
                    let prevX = 0 - nextW;
                    let nextX = this.game.width;
                    let velocity = this.dayLenght;
                    let easing = Phaser.Easing.Default;
                    if (isRightToLeft) {
                        prevX = this.game.width + nextW;
                        nextX = 0 - nextW;
                        easing = Phaser.Easing.Circular.InOut;
                    }

                    this.autobus = this.game.add.sprite(prevX, this.game.height - nextH, "autobus1");
                    this.autobus.width = nextW;
                    this.autobus.height = nextH;
                    this.autobus.anchor.set(isRightToLeft ? 1 : 0, this.autobus.anchor.y);

                    let autobusTween = this.game.add.tween(this.autobus).to({ x: nextX }, velocity, easing, true)
                        .onComplete.add((s: Phaser.Sprite, t: any) => {
                            s.destroy();
                        });
                }

            }

        }

        protected intInRange(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        private spawnTraffic(options: UpdateTrafficOptions) {
            let chance = this.intInRange(1, 1000) < 20;
            for (let i = 0; i < options.intervals.length; i++) {
                if (options.intervals[i].condition()) {
                    chance = this.intInRange(1, 1000) < options.intervals[i].limit;

                }
            }

            if (chance) {
                let isRightToLeft = this.intInRange(1, 2) === 2;
                let selector = "car" + this.intInRange(1, 4);
                let car = this.game.cache.getImage(selector);
                let nextH = options.putOnTop ? 40 : 30;
                let nextW = (car.width * nextH) / car.height;
                let prevX = 0;
                let nextX = this.game.width;
                let speed = this.dayLenght;
                let easing = Phaser.Easing.Default;
                let anchorX = 0;
                let scaleX = 1;

                if (isRightToLeft) {
                    prevX = this.game.width + nextW;
                    nextX = 0 - nextW;
                    anchorX = 1;
                } else {
                    prevX -= nextW;
                    nextX += nextW;
                    scaleX = -1;
                }

                let carSprite = new CarBuilder(this.game, this.utils, this.dayLenght, new CarOptions(
                    prevX,
                    this.game.height - nextH,
                    anchorX,
                    nextX,
                    nextW,
                    nextH,
                    scaleX,
                    options.putOnTop,
                    speed,
                    easing,
                    selector
                )).Create();

                if (options.putOnTop) {
                    this.carSprites.push(carSprite);
                }

                // todo: speed should be ony multiply of 10 and not based of daylight(too slow)
                // todo: easing should be random from a pool of predefined: overridable
                let carTween = this.game.add.tween(carSprite).to({ x: nextX }, speed, easing, true)
                    .onComplete.add((s: Phaser.Sprite, t: any) => {
                        s.destroy();
                    });
            }
        }

        Update(date: Date, hour: number) {
            this.spawn56(date, hour);

            //let backTrafficOptions = new UpdateTrafficOptions(date, hour, false);
            //backTrafficOptions.intervals.push(new TrafficIntervall(() => true, 100));
            //this.spawnTraffic(backTrafficOptions);

            let trafficOptions = new UpdateTrafficOptions(date, hour, true);
            trafficOptions.intervals.push(new TrafficIntervall(function () { return (hour === 9) || (hour === 18); }, 150));
            this.spawnTraffic(trafficOptions);
        }

        destroyAutobus(autobus: Phaser.Sprite) {
            autobus.destroy(true);

        }

        putOnTop() {
            if (this.autobus !== null) {
                this.game.world.bringToTop(this.autobus);
            }

            for (var i = 0; i < this.carSprites.length; i++) {
                if (this.carSprites[i] != null) {
                    this.game.world.bringToTop(this.carSprites[i]);
                }

            }
        }

    }
    
}