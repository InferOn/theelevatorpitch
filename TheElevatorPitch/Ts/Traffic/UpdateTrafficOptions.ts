﻿namespace TheElevatorPitchModule {
    "use strict";
    
    export class UpdateTrafficOptions {
        public date: Date;
        public hour: number;
        public putOnTop: boolean;
        public intervals: TrafficIntervall[];
        constructor(date: Date, hour: number, putOnTop: boolean) {
            this.date = date;
            this.hour = hour;
            this.putOnTop = putOnTop;
            this.intervals = [];
        }
    }
    
}