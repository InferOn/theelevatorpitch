﻿namespace TheElevatorPitchModule {
    "use strict";

    export class Utils {
        private game: Phaser.Game;

        constructor(game: Phaser.Game) {
            this.game = game;
        }

        tweenTint(sprite: Phaser.Sprite, startColor: number, endColor: number, duration: number, easing: any) {
            let colorBlend = { step: 0 };

            this.game.add.tween(colorBlend).to({ step: 100 }, duration, easing ? easing : Phaser.Easing.Default, false)
                .onUpdateCallback(() => {
                    sprite.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step, 1);
                })
                .start();
        }

        RGBtoHEX(r: number, g: number, b: number) {
            return r << 16 | g << 8 | b;
        }

        capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    }
}
