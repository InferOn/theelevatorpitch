﻿/// <reference path="Ts/Boot.ts" />

namespace TheElevatorPitchModule {
    class TheElevatorPitch extends Phaser.Game {
        constructor() {
            super("100%", "100%", Phaser.AUTO, 'content', null);

            this.state.add('Boot', Boot, false);
            this.state.add('TheCity', TheCity, false);

            this.state.start("Boot", false, false);
            try {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

            }
            catch (ex){
                console.error(ex);
            }

        }
    }

    window.onload = () => {
        var game = new TheElevatorPitch();
    };
}
